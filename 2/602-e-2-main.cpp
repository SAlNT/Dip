#include <bits/stdc++.h>

using namespace std;

#define forn(i,n) for(int i = 0; i < (n); i++)
#define fornr(i,n) for(int i = (n) - 1; i >= 0; i--)
#define forab(i,a,b) for(int i = (a); i < (b); i++)
#define mp make_pair
#define pb push_back
#define sz(X) int((X).size())
#define all(X) (X).begin(),(X).end()
#define fst first
#define snd second

using ll = long long;
using pii = pair<int,int>;
using vi = vector<int>;

const int N = 2e5 + 100;

vi g[N];
int got[N], centerV = -1, centerDist;
int x, farV = -1, farDist = -1;
vector<int> st;
	
void dfsMax(int v, int p, int dist) {
	if (got[v] && dist > farDist) {
		farDist = dist;
		farV = v;
	}
	for (int to : g[v]) {
		if (to != p) {
			dfsMax(to, v, dist + 1);
		}
	}
}

bool findCenter(int v, int p, int endpoint) {
	st.pb(v);
	if (v == endpoint) {
		int dist = sz(st) - 1;
		if (dist % 2 != 0) {
			cout << "NO\n";
			exit(0);
		}
		centerV = st[dist / 2];	
		centerDist = dist / 2;
		return true;
	}
	for(int to : g[v]) {
		if (to != p) {
			if (findCenter(to, v, endpoint)) {
				return true;
			}
		}
	}
	st.pop_back();
	return false;
}

void dfsCheck(int v, int p, int dist) {
	if (got[v] && dist != centerDist) {
		cout << "NO\n";
		exit(0);
	}
	for(int to : g[v]) {
		if (to != p) {
			dfsCheck(to, v, dist + 1);
		}
	}
}


int main() {
#ifdef LOCAL
	freopen(".in", "r", stdin);
	freopen(".out", "w", stdout);
#endif
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	int n, m;
	cin >> n >> m;
	forn(i, n - 1) {
		int v, u;
		cin >> v >> u;
		v--, u--;
		g[v].pb(u);
		g[u].pb(v);
	}
	forn(i, m) {
		cin >> x;
		x--;
		got[x] = 1;
	}
	if (m == 1) {
		cout << "YES\n";
		cout << x + 1 << '\n';
		return 0;
	}
	dfsMax(x, x, 0);
	int d1 = farV;
	farV = -1, farDist = -1;
	dfsMax(d1, d1, 0);
	int d2 = farV;
	findCenter(d1, d1, d2);
	assert(centerV != -1);
	dfsCheck(centerV, centerV, 0);
	cout << "YES\n";
	cout << centerV + 1 << '\n';	
	
	return 0;
}
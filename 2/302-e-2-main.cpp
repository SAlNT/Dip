#include <bits/stdc++.h>
#pragma optimize("O3")
#define int long long
#define ld long double
#define pb push_back
#define all(v) v.begin(), v.end()
#define rep(i, n) for(int i = 0; i < n; i++)

using namespace std;

bool is[200005];
int used[200005];
int bylo[200005];
vector<int> a[200005];
vector<int> c;

int dfs(int v, int otec){
    if(used[v])
        return 0;
    used[v] = true;
    if(otec == -1){
        int kol = 0;
        vector<int> nw;
        for(int x : a[v]){
            int k = dfs(x, v);
            if(k > 0){
                kol++;
                nw.pb(x);
            }
        }
        if(kol > 1){
            cout << "NO\n";
            exit(0);
        }
        swap(a[v], nw);
        return 0;
    }
    if(is[v]){
        a[v].clear();
        a[v].pb(otec);
        return 1;
    }else{
        int sum = 0;
        vector<int> nw;
        for(int x : a[v]){
            if(!used[x]){
                int t = dfs(x, v);
                if(t != 0)
                    nw.pb(x);
                sum += t;
            }
        }
        nw.pb(otec);
        swap(nw, a[v]);
        return sum;
    }
}

void zhi(){
    vector<int> nw;
    for(int v : c){
        if(a[v].size() > 1 || is[a[v][0]]){
            cout << "NO\n";
            exit(0);
        }
        bylo[v] = 14;
        int otec = a[v][0];
        if(used[otec] != INT_MAX){
            used[otec] = INT_MAX;
            nw.pb(otec);
        }
    }
    for(int x : nw){
        vector<int> peacedeath;
        is[x] = true;
        for(int h : a[x]){
            if(bylo[h] != 14){
                peacedeath.pb(h);
            }
        }
        if(peacedeath.size() > 1){
            cout << "NO\n";
            exit(0);
        }
        swap(a[x], peacedeath);
    }
    swap(c, nw);
}

int32_t main() {
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    cout.precision(10);
    fill(is, is + 200005, false);
    fill(used, used + 200005, false);
    fill(bylo, bylo + 200005, false);
    int n, m;
    cin >> n >> m;
    rep(i, n-1){
        int x, y;
        cin >> x >> y;
        x--, y--;
        a[x].pb(y);
        a[y].pb(x);
    }
    rep(i, m){
        int x;
        cin >> x;
        c.pb(x-1);
        is[x-1] = true;
    }
    if(m == 1){
        cout << "YES\n";
        cout << c[0] + 1;
        return 0;
    }
    dfs(c[0], -1);
//    rep(i, n){
//        for(int x : a[i])
//            cout << i+1 << " " << x+1 << '\n';
//    }
    while(true){
        zhi();
        if(c.size() == 1){
            cout << "YES\n";
            cout << c[0]+1;
            return 0;
        }
    }
}
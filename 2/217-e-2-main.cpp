#include <bits/stdc++.h>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
#ifdef HOME
    freopen("in.txt", "r", stdin);
#endif
    int n, m;
    cin >> n >> m;
    vector<vector<int>> g(n);
    for (int i = 0; i < n - 1; i++) {
        int x, y;
        cin >> x >> y;
        x--, y--;
        g[x].push_back(y);
        g[y].push_back(x);
    }
    if (m == 1) {
        int x;
        cin >> x;
        cout << "YES\n";
        cout << x << endl;
        return 0;
    }
    queue<int> q;
    vector<pair<int, int>> dist(n, make_pair(-1, -1));
    for (int i = 0; i < m; i++) {
        int x;
        cin >> x;
        q.push(x - 1);
        dist[x - 1] = make_pair(0, 1);
    }
    while (!q.empty()) {
        int v = q.front();
        q.pop();
        for (int to : g[v]) {
            if (dist[to].first == -1) {
                dist[to] = make_pair(dist[v].first + 1, dist[v].second);
                q.push(to);
            } else {
                if (dist[to].first == dist[v].first + 1) {
                    dist[to].second += dist[v].second;
                }
            }
        }
    }
    for (int i = 0; i < n; i++) {
        if (dist[i].second == m) {
            cout << "YES\n" << i + 1 << '\n';
            return 0;
        }
    }
    cout << "NO\n";
}
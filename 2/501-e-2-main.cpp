#include <bits/stdc++.h>

#define all(x) begin(x), end(x)
#define sz(x) ((int)x.size())

using namespace std;
using ll = long long;
using ld = long double;
const int inf = 1e9 + 10;

int main() {
#ifdef local
    assert(freopen("../in.txt", "r", stdin));
#endif
    ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);
    int n, m;
    cin >> n >> m;
    if (n == 1) {
        cout << "YES\n1";
        return 0;
    }
    vector<int> g[n];
    int d[n], col[n];
    bool marked[n] = {};
    for (int i = 0; i < n - 1; i++) {
        int u, v;
        cin >> u >> v;
        u--;
        v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }
    for (int i = 0; i < m; i++) {
        int c;
        cin >> c;
        c--;
        marked[c] = true;
    }
    set<pair<int, pair<int, int> > > nm, ma;
    for (int i = 0; i < n; i++) {
        d[i] = g[i].size();
        if (marked[i]) {
            col[i] = 0;
            ma.insert(make_pair(g[i].size(), make_pair(col[i], i)));
        } else {
            nm.insert(make_pair(g[i].size(), make_pair(col[i], i)));
        }
    }
    while (!nm.empty()) {
        pair<int, pair<int, int>> p = *nm.begin();
        if (p.first != 1) {
            break;
        }
        d[p.second.second] = 0;
        nm.erase(nm.begin());
        for (int j = 0; j < g[p.second.second].size(); j++) {
            int kek = g[p.second.second][j];
            if (d[kek] > 0) {
                if (marked[kek]) {
                    ma.erase(make_pair(d[kek], make_pair(col[kek], kek)));
                } else {
                    nm.erase(make_pair(d[kek], make_pair(col[kek], kek)));
                }
                d[kek]--;
                if (marked[kek]) {
                    ma.insert(make_pair(d[kek], make_pair(col[kek], kek)));
                } else {
                    nm.insert(make_pair(d[kek], make_pair(col[kek], kek)));
                }
            }
        }
    }
    while (ma.size() > 1) {
        pair<int, pair<int, int> > p = *ma.begin();
        d[p.second.second] = 0;
        ma.erase(ma.begin());
        for (int j = 0; j < g[p.second.second].size(); j++) {
            int kek = g[p.second.second][j];
            if (d[kek] > 0) {
                if (marked[kek]) {
                    if (col[kek] != col[p.second.second] + 1) {
                        cout << "NO";
                        return 0;
                    }
                    ma.erase(make_pair(d[kek], make_pair(col[kek], kek)));
                } else {
                    nm.erase(make_pair(d[kek], make_pair(col[kek], kek)));
                }
                marked[kek] = true;
                col[kek] = col[p.second.second] + 1;
                d[kek]--;
                ma.insert(make_pair(d[kek], make_pair(col[kek], kek)));
            }
        }
    }
    cout << "YES\n" << (*ma.begin()).second.second + 1;
}
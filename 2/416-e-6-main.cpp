#include <bits/stdc++.h>

using namespace std;

const int maxn = 2 * 1e5 + 3;
vector<int> edges[maxn];
int teams[maxn];
int dist[maxn];
int ndist[maxn];
vector<bool> was;

int main()
{
    // freopen("test.in", "r", stdin);
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n, m, x, y;
    scanf("%d %d", &n, &m);
    // cin >> n >> m;
    for (int i = 0; i < n - 1; i++) {
        scanf("%d %d", &x, &y);
        // cin >> x >> y;
        edges[x - 1].push_back(y - 1);
        edges[y - 1].push_back(x - 1);
    }

    for (int i = 0; i < m; i++) {
        scanf("%d", &(teams[i]));
        // cin >> teams[i];
        teams[i]--;
        edges[n].push_back(teams[i]);
        // edges[teams[i]].push_back(n);
    }

    was.resize(n + 1);
    for (int i = 0; i < n; i++) {
        was[i] = false;
    }
    deque<int> q;
    q.push_back(n);
    was[n] = true;
    ndist[n] = 1;

    while (!q.empty()) {
        int cur = q.front();
        q.pop_front();
        for (auto next : edges[cur]) {
            if (was[next] && dist[next] == dist[cur] + 1) {
                ndist[next] += ndist[cur];
            }
            if (!was[next]) {
                was[next] = true;
                q.push_back(next);
                dist[next] = dist[cur] + 1;
                ndist[next] = ndist[cur];
            }
        }
    }

    bool ans = false;
    int ansv = -1;

    for (int i = 0; i < n; i++) {
        if (ndist[i] == m) {
            ans = true;
            ansv = i + 1;
        }
    }

    if (!ans) {
        cout << "NO";
    } else {
        cout << "YES" << endl << ansv;
    }
    return 0;
}

#include <bits/stdc++.h>

/* ============================================================= */

template <class ...Args>
bool read(Args& ...args)
{ return (bool)(std::cin >> ... >> args); }

template <class ...Args>
void write(Args&& ...args)
{ (std::cout << ... << std::forward<Args>(args)); }

template <class ...Args>
void writeln(Args&& ...args)
{ (std::cout << ... << std::forward<Args>(args)) << '\n'; }

/* ============================================================= */

enum {
    UNSET=-3,
    NONE=-2,
    ERR=-1,
};

std::vector<std::vector<int>> es;
std::vector<char> mark;
std::vector<int> dp_mn, dp_mx;

void calc_dp(int v, int p) {
    if (mark[v]) {
        dp_mn[v] = std::min(dp_mn[v], 0);
        dp_mx[v] = std::max(dp_mx[v], 0);
    }
    for (int u : es[v]) {
        if (u == p) continue;
        calc_dp(u, v);
        dp_mn[v] = std::min(dp_mn[v], dp_mn[u]+1);
        dp_mx[v] = std::max(dp_mx[v], dp_mx[u]+1);
    }
}

int valid = -1;
void go(int v, int p, int mn, int mx) {
    if (std::min(mn, dp_mn[v]) == std::max(mx, dp_mx[v])) // ALL EDGES EQUALS
        valid = v;

    if (mark[v]) { // don't use dp[v] from here
        mn = std::min(mn, 0);
        mx = std::max(mx, 0);
    }

    if (es[v].size() + (p == -1) == 1)
        return;

    int mn1 = mn, mn2 = 3*es.size(); // mn1 < mn2
    int mx1 = mx, mx2 = -3*es.size(); // mx1 > mx2
    for (int u : es[v]) {
        if (u == p) continue;
        if (dp_mn[u]+1 < mn2) {
            mn2 = dp_mn[u]+1;
            if (mn2 < mn1) std::swap(mn1, mn2);
        }
        if (dp_mx[u]+1 > mx2) {
            mx2 = dp_mx[u]+1;
            if (mx2 > mx1) std::swap(mx1, mx2);
        }
    }

    for (int u : es[v]) {
        if (u == p) continue;
        int cur_mn = (dp_mn[u]+1 == mn1) ? mn2 : mn1;
        int cur_mx = (dp_mx[u]+1 == mx1) ? mx2 : mx1;
        go(u, v, cur_mn+1, cur_mx+1);
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    std::fixed(std::cout).precision(10);

    int n, m; read(n, m);

    es.resize(n);
    for (int i = 0; i < n-1; ++i) {
        int a, b; read(a, b); --a, --b;
        es[a].push_back(b);
        es[b].push_back(a);
    }

    mark.resize(n);
    for (int i = 0; i < m; ++i) {
        int x; read(x); --x;
        mark[x] = true;
    }

    dp_mn.resize(n, n * 3);
    dp_mx.resize(n, -n * 3);
    calc_dp(0, -1);

    go(0, -1, n*3, -n*3);
    if (valid != -1) {
        writeln("YES");
        writeln(valid + 1);
    } else {
        writeln("NO");
    }


    return 0;
}
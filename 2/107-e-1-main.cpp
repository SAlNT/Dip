#include <bits/stdc++.h>

using namespace std;
int Common = -1;
int need_dist = -1;
bool flag = false;
int ans = 0;
int start = -1;
int max_path_length = -1;
int dfs(int cur, vector<vector<int> >& g, unordered_set<int>& cities, vector<bool>& visited, int dist, vector<int>& posl){
    visited[cur] = true;
    if (start != cur && cities.find(cur) != cities.end()){
        if (dist % 2 == 1){
            ans = -1;
            return -1;
        }
        if (dist > max_path_length){
            max_path_length = dist;
            Common = posl[dist/2];
            need_dist = dist / 2;
        }
    }
        for (auto el : g[cur]){
            if (!visited[el]){
                posl[dist+1] = el;
                int res = dfs(el, g, cities, visited, dist + 1, posl);
                if (res == -1)
                    return -1;
            }
        }

}

bool dfs2(int cur, int dist, int need_dist, vector<vector<int> >& g, unordered_set<int>& cities, vector<bool>& visited){
    visited[cur] = true;
    //cout << cur << endl;
    if (cities.find(cur) != cities.end()){
        //cout << cur << " " << dist << endl;
        if (need_dist == dist){
            return true;
        } else {
            return false;
        }
    }
    bool res = true;
    for (auto el : g[cur]){

        if (!visited[el]){
            res &= dfs2(el, dist+1, need_dist, g, cities, visited);

        }
    }
    return res;

}

int main() {
    int n, m;
    cin >> n >> m;
    int u, v;
    vector<vector<int> >g(n);
    for (int i = 0; i < n-1; i++){
        cin >> u >> v;
        g[u-1].push_back(v-1);
        g[v-1].push_back(u-1);
    }
    int team_city;
    unordered_set<int> cities;
    //vector<bool> cities_vec(m,-1);
    for (int i = 0; i < m; i++){
        cin >> team_city;
        cities.insert(team_city-1);
        //cities_vec[i] = team_city -1;
    }
    if ((int) cities.size() == 1){
        cout << "YES" << endl;
        cout << (*cities.begin()) + 1 << endl;
        return 0;
    }
    vector<bool> visited(n, false);
    //start = cities_vec[0];
    start = *cities.begin();
    vector<int> posl(n, -1);
    posl[0] = start;
    dfs(start, g, cities, visited, 0, posl);
    if (ans == -1){
        cout << "NO" << endl;
        return 0;
    }
    if (Common != -1) {
        visited = vector<bool>(n, false);
        bool answer = dfs2(Common, 0, need_dist, g, cities, visited);
        if (answer){
            cout << "YES" << endl;
            cout << Common + 1 << endl;
        } else {
            cout << "NO" << endl;
        }
    }
    else
        cout << "NO" << endl;
    return 0;
}
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>

using namespace std;

int n, m;

vector<vector<int>> graph;
vector<int> teams;

vector<int> len;

vector<int> dfs_u, dfs_v, dfs_f;

void dfs_rec(int u)
{
	for (int f : graph[u])
		if (len[f] == -1)
		{
			len[f] = len[u] + 1;
			dfs_rec(f);
		}
}

void dfs(int u)
{
	len.resize(n + 1);

	for (int i = 0; i <= n; i++)
		len[i] = -1;

	len[u] = 0;

	dfs_rec(u);
}

int main()
{
	cin >> n >> m;

	graph.resize(n + 1);

	for (int i = 0; i < (n - 1); i++)
	{
		int u, v;

		scanf("%d %d", &u, &v);

		graph[u].push_back(v);
		graph[v].push_back(u);
	}

	teams.resize(m);

	for (int i = 0; i < m; i++)
		scanf("%d", &(teams[i]));

	if (m == 1)
	{
		cout << "YES" << endl << teams[0] << endl;

		return 0;
	}

	dfs(teams[min(m - 1, 3)]);
	dfs_u = len;

	int mv = teams[0];

	for (int i = 1; i < m; i++)
		if (dfs_u[teams[i]] > dfs_u[mv])
			mv = teams[i];

	dfs(mv);
	dfs_v = len;

	for (int i = 1; i <= n; i++)
	{
		if (dfs_u[i] == dfs_v[i])
		{
			int l = dfs_u[i];

			dfs(i);
			dfs_f = len;

			bool flag = true;

			for (int t : teams)
				if (dfs_f[t] != l)
				{
					flag = false;

					break;
				}

			if (flag)
			{
				cout << "YES" << endl << i << endl;

				return 0;
			}
		}
	}

	cout << "NO" << endl;

	return 0;
}
#include <iostream>
#include <vector>
#include <cstdio>
#include <set>
#include <algorithm>

#define fork(i, n, step) for(int i=0; i<n; i+=step)

typedef unsigned long long ull;
typedef long long ll;

using namespace std;

ll Abs(ll a) {
	if (a > 0) return a;
	else return - a;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	ll n;
	cin >> n; 

	ll d = (1 >> 60);
	ll u = -(1 >> 60);
	ll l = (1 >> 60);
	ll r = -(1 >> 60);

	ll x, y, h;
	ll td, tu, tl, tr;
	if (n == 1) {
		cin >> x >> y >> h;
		cout << x << ' ' << y << ' ' << h << endl;
		return 0;
	}

	for(int i = 0; i < n; i++) {
		cin >> x >> y >> h;
		
		tu = x + h;
		td = x - h;

		tr = y + h;
		tl = y - h;

		if (tu > u) u = tu;
		if (tr > r) r = tr;
		if (tl < l) l = tl;
		if (td < d) d = td;
	}

	ll a = (u - d);
	ll b = (r - l);

	ll k = max(a, b);
	if (k % 2 != 0) {
		k = k / 2 + 1;
		d--;
		l--;
	}
	else k = k / 2;
	k = abs(k);

	x = d + k;
	y = l + k;

	cout << x << ' ' << y << ' ' << k << endl;

	return 0;
}
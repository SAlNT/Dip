#include <iostream>
#include <vector>
#include <deque>
#include <unordered_map>

using namespace std;

typedef long long int ll;

const int MAXN = 200005;
vector<int> arr[MAXN];
vector<int> teams;
int dist[MAXN];
bool used[MAXN];
int weight[MAXN];

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < n - 1; ++i) {
        int x, y;
        cin >> x >> y;
        arr[x].push_back(y);
        arr[y].push_back(x);
    }
    deque<int> bfs;
    for (int i = 0; i < m; ++i) {
        int x;
        cin >> x;
        teams.push_back(x);
        bfs.push_back(x);
        used[x] = true;
        weight[x] = 1;
    }
    if (m == 1) {
        cout << "YES\n";
        cout << teams[0];
        return 0;
    }
    while (!bfs.empty()) {
        auto p = bfs.front();
        bfs.pop_front();
        int v = p;
        for (int to : arr[v]) {
            if (!used[to]) {
                dist[to] = dist[v] + 1;
                used[to] = true;
                bfs.push_back(to);
            }
            if (dist[to] == dist[v] + 1) {
                weight[to] += weight[v];
            }
        }
    }
    for (int i = 1; i <= n; ++i) {
        if (weight[i] == m) {
            cout << "YES\n";
            cout << i;
            return 0;
        }
    }

    cout << "NO";
    return 0;
}
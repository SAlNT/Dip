#include <bits/stdc++.h>
#define pii pair<int, int>
using namespace std;

const int N = 3 * (int)1e5;

vector<int> g[N];

int used[N];

int lg[N];

int sm[N];

int ans = 0;

int mark[N];

int sz[N];

int d[N];

void dfs(int x) {
    used[x] = 1;
    sz[x] = 0;
    if(mark[x] == 1) {
        if(ans == -1) return;
        ans = x;
        sz[x] = 1;
    }
    vector<int> v;
    for(int i = 0; i < g[x].size(); ++i) {
        int u = g[x][i];
        if(used[u]) continue;
        dfs(u);
        if(ans == -1) return;
        sz[x] += sz[u];
        if(d[u] == 1) {
            v.push_back(i);
        }
    }
    while(!v.empty()) {
        swap(g[x][v.back()], g[x][g[x].size() - 1]);
        g[x].pop_back();
        v.pop_back();
    }
    if(sz[x] == 0) {
        d[x] = 1;
        g[x].clear();
    }
    if(mark[x] && g[x].size() > 1) {
        ans = -1;
    }
}

int ansx = -1;

void centroid(int x) {
    used[x] = 1;
    if(mark[x]) {
        lg[x] = 0;
    }
    for(int u : g[x]) {
        if(used[u]) continue;
        lg[u] = lg[x] + 1;
        centroid(u);
        lg[x] = min(lg[x], lg[u] + 1);
    }
    if(ansx < lg[x]) {
        ansx = lg[x];
        ans = x;
    }
}

int r[N];

void f(int x) {
    used[x] = 1;
    if(mark[x]) {
        r[x] = 0;
    }
    for(int u : g[x]) {
        if(used[u]) continue;
        f(u);
        r[x] = max(r[x], r[u] + 1);
    }
    if(x == ans) {
        if(r[x] != lg[x]) {
            ans = -1;
        }
        if(mark[x]) {
            ans = -1;
        }
    }
}

int main() {
    ios::sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    for(int i = 1; i < n; ++i) {
        int u, v;
        cin >> u >> v;
        u--, v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }
    for(int i = 0; i < m; ++i) {
        int t;
        cin >> t;
        t--;
        mark[t] = 1;
    }
    if(m == 1) {
        cout << "YES\n";
        for(int i = 0 ;i < n; ++i) {
            if(mark[i] == 1) {
                cout << i + 1;
            }
        }
        return 0;
    }
    dfs(0);
    if(ans == -1) {
        return cout << "NO", 0;
    }
    fill(used, used + n, 0);
    int y = ans;
    ans = -1;
    centroid(y);
    if(ans == -1) {
        return cout << "NO", 0;
    }
    fill(used, used + n, 0);
    f(ans);
    if(ans == -1) {
        return cout << "NO", 0;
    }
    cout << "YES\n";
    cout << ans + 1;
    return 0;
}

/*
6 3
1 2
2 3
3 4
4 5
4 6
1 5 6

2 2
1 2
1 2

7 5
1 2
2 3
2 4
1 5
5 6
5 7
1 3 4 6 7

9 2
4 5
5 1
1 9
1 2
2 7
2 3
3 8
3 6
1 3
*/


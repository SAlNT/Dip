#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <map>

using namespace std;
using vint = vector <int>;

const int INF = 2000000000;

vint used, teams, dist, nums;
vector <vint> d;

map <int, int> cont;
map <int, bool> rel;

int main() {
	// ...
	ios::sync_with_stdio(false);
	int n, m, a, b, id(-1);
	cin >> n >> m;

	d.resize(n);
	dist.resize(n);
	used.resize(n);
	teams.resize(m);
	nums.resize(n);

	for (int i(0); i < n - 1; i++) {
		cin >> a >> b;

		--a, --b;
		d[a].push_back(b);
		d[b].push_back(a);
	}

	for (auto& i : teams) {
		cin >> i;
		--i;
		rel[i] = true;
	}

	//
	if (m == 1) {
		cout << "YES\n" << 1;
		return 0;
	}

	if (m == n) {
		cout << "NO\n";
		return 0;
	}

	// refr
	queue <int> q;

	for (auto i : teams) {
		q.push(i);
		used[i] = true;
	}

	while (!q.empty()) {
		int curr = q.front(); q.pop();
		used[curr] = true;

		for (auto next : d[curr]) {
			if (rel[next] && rel[curr]) {
				cout << "NO";
				return 0;
			}
			if (!used[next]) {
				if (dist[next]) {
					if (dist[next] == dist[curr] + 1) {
						nums[next]++;
					}
					else {
						cout << "NO";
						return 0;
					}
				}

				if (!dist[next]) {
					dist[next] = dist[curr] + 1;

					cont[dist[next]]++;
					q.push(next);
				}
			}
		}
	}

	int y = -1, mx(0);
	for (int i(0); i < n; i++) {
		if (nums[i] == m - 1) {
			cout << "YES\n" << i+1;
			return 0;
		}
	}

	cout << "NO";
}
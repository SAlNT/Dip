#include <bits/stdc++.h>

#define sz(a) ((int)((a).size()))
#define char unsigned char
#define X first
#define x first
#define Y second
#define y second

using namespace std;

typedef long long ll;
typedef long double ld;

const int N = 212345;

vector<int> g[N];

int to;

vector<int> st;
int is[N];
int dist[N];
bool has[N];
bool many[N];


void dfs(int v, int pr)
{
    int curdist = -1;
    if (is[v])
    {
        curdist = 0;
        has[v] = 1;
    }
    for (int u : g[v])
    {
        if (u != pr)
        {
            dfs(u, v);
            if (has[u])
            {
                has[v] = 1;
                if (many[u])
                {
                    many[v] = 1;
                }
                else if (curdist != -1 && curdist != dist[u] + 1)
                {
                    many[v] = 1;
                }
                else if (curdist == -1)
                {
                    curdist = dist[u] + 1;
                }
            }
        }
    }
    dist[v] = curdist;
}

bool good = true;

int dfs2(int v, int pr)
{
    int cur_d = -1;
    if (is[v])
        cur_d = 0;
    for (int u : g[v])
        if (u != pr)
        {
            int d = dfs2(u, v) + 1;
            if (!good)
                return 0;
            if (d == 0)
                continue;
            if (cur_d != -1 && d != -1 && cur_d != d)
            {
                good = false;
                return 0;
            }
            if (d != -1)
                cur_d = d;
        }
    return cur_d;
}

int main()
{
#ifdef ONPC
    freopen("in.txt", "r", stdin);
#endif // ONPC
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, m;
    cin >> n >> m;
    for (int i = 1; i < n; i++)
    {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    vector<int> c(m);
    for (int i = 0; i < m; i++)
    {
        cin >> c[i];
        c[i]--;
        is[c[i]] = 1;
    }
    if (m == 1)
    {
        cout << "YES\n" << c[0] + 1 << endl;
        return 0;
    }
    if (m == n)
    {
        cout << "NO\n";
        return 0;
    }
    int root = 0;
    while (is[root])
        root++;
    dfs(root, root);
    int cur = root;
    int pr = -1;
    int cur_dist = -1;
    while (true)
    {

        int v = cur;
        int need_go = -1;
        bool has_many = false;
        bool need_to_go = false;
        int new_dist = cur_dist;
        for (int u : g[v])
            if (u != pr)
            {
                if (has[u])
                {
                    if (many[u])
                        need_to_go = true;
                    if (new_dist == -1)
                    {
                        new_dist = dist[u] + 1;
                    }
                    else if (new_dist != dist[u] + 1)
                        need_to_go = true;
                }
            }
        if (!need_to_go)
            break;
        for (int u : g[v])
            if (u !=  pr && many[u])
                has_many = true;
        if (cur_dist == -1 && !has_many)
        {
            int dst = -1;
            for (int u : g[v])
                if (u != pr)
                {
                    if (has[u] && dist[u] > dst)
                    {
                        dst = dist[u];
                        need_go = u;
                    }
                }
        }
        else
        {
            for (int u : g[v])
                if (u != pr)
                {
                    if (has[u])
                    {
                        if (many[u] || (cur_dist != -1 && dist[u] + 1 != cur_dist))
                        {
                            if (need_go == -1)
                            {
                                need_go = u;
                            }
                            else
                            {
                                cout << "NO\n";
                                return 0;
                            }
                        }
                    }
                }
        }
        for (int u : g[v])
            if (u != pr && u != need_go)
            {
                if (has[u])
                {
                    if (many[u] || (cur_dist != -1 && cur_dist != dist[u] + 1))
                    {
                        cout << "NO\n";
                        return 0;
                    }
                    cur_dist = dist[u] + 1;
                }
            }
        if (need_go == -1)
            break;

        if (cur_dist != -1)
            cur_dist++;
        if (is[need_go])
        {
            cur_dist = 0;
        }
        pr = cur;
        cur = need_go;
    }
    dfs2(cur, cur);
    if (good)
    {
        cout << "YES\n";
        cout << cur + 1 << endl;
        return 0;
    }
    else
        cout << "NO\n";
    return 0;
}

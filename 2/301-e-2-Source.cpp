#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <vector>
#include <bitset>
#include <stack>
#include <assert.h>
#include <queue>
#include <cstdio>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <random>
#include <iomanip>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <string>

using namespace std;

#define pb push_back
#define eb emplace_back
#define all(x) (x).begin(), (x).end()
#define len(x) (int)(x).size()

using ll = long long;
using ld = long double;

const ld pi = acos(-1.0L);
const ld eps = 0.00001L;

template<typename T, typename TT> void mi(T& a, TT b) {
	if (b < a)a = b;
}
template<typename T, typename TT> void ma(T& a, TT b) {
	if (b > a)a = b;
}

int n, m;
vector<int> g[200005];
int color[200005];
int here[200005];
int cnt[200005];
int used[200005];
vector<int> ng[200005];
int hh[200005];

void dfs(int v, int rt = -1) {
	cnt[v] = color[v];
	used[v] = 1;
	vector<int> have;
	vector<int> good;
	for (auto& x : g[v]) {
		if (used[x])continue;
		dfs(x, v);
		if (cnt[x]) {
			have.pb(x);
		}
		else {
			good.pb(x);
		}
		cnt[v] += cnt[x];
	}
	if (color[v] == 1) {
		if (rt == -1 && len(have) >= 2) {
			cout << "NO";
			exit(0);
		}
		if (rt != -1 && len(have) >= 1) {
			cout << "NO";
			exit(0);
		}
	}
	for (auto& x : good) {
		here[x] = 1;
	}
}

void rest(int v) {
	used[v] = 1;
	for (auto& x : g[v]) {
		if (used[x])continue;
		rest(x);
		if (!here[x]) {
			ng[v].pb(x);
			ng[x].pb(v);
		}
	}
}

void dfst(int v, int h = 0) {
	hh[v] = h;
	used[v] = 1;
	for (auto& x : g[v]) {
		if(used[x])continue;
		dfst(x, h + 1);
	}
}

int nr = -1;
vector<int> way;
int good = 1;
void dfstt(int v, int p) {
	used[v] = 1;
	if (v == nr) {
		good = 0;
		way.pb(v);
	}
	for (auto& x : g[v]) {
		if (used[x])continue;
		dfstt(x, v);
		if (good == 0) {
			way.pb(v);
			return;
		}
	}
}

int main() {
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif

	cin >> n >> m;
	if (m == 1) {
		cout << "YES\n1";
		return 0;
	}
	for (int i = 0; i < n-1; i++) {
		int a, b;
		cin >> a >> b;
		a--;
		b--;
		g[a].pb(b);
		g[b].pb(a);

	}
	for (int i = 0; i < m; i++) {
		int a;
		cin >> a;
		a--;
		color[a] = 1;
	}

	int root = 0;
	while (color[root] == 0)root++;
	dfs(root);

	for (auto& x : used)x = 0;

	rest(root);

	for (int i = 0; i < 200005; i++) {
		g[i] = ng[i];
	}
	for (auto& x : used)x = 0;

	dfst(root);
	int mx = -1;
	for (int i = 0; i < n; i++) {
		if (hh[i] > mx) {
			mx = hh[i];
			nr = i;
		}
	}

	if (mx & 1) {
		cout << "NO";
		return 0;
	}

	for (auto& x : used)x = 0;
	
	dfstt(root, -1);

	int v = way[mx / 2];

	for (auto& x : used)x = 0;
	for (auto& x : hh)x = 0;
	

	dfst(v);
	assert(color[v] == 0);
	int d = -1;
	for (int j = 0; j < n; j++) {
		if (color[j]) {
			if (d == -1) {
				d = hh[j];
			}
			else {
				if (d != hh[j]) {
					cout << "NO";
					return 0;
				}
			}
		}
	}
	cout << "YES\n" << v + 1;


	return 0;
}

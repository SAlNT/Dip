#include <bits/stdc++.h>
#define ll long long
#define ld long double
using namespace std;
vector<vector<ll>> g;
vector<ll> c, tin, tout, d;
vector<vector<ll>> p;
ll timer = 0;
void dfs(ll v, ll pred)
{
    tin[v] = timer++;
    for(int i = 0; i < g[v].size(); i++)
    {
        ll to = g[v][i];
        if(to != pred)
        {
            p[to].push_back(v);
            d[to] = d[v] + 1;
            dfs(to, v);
        }
    }
    tout[v] = timer++;
}
bool f(ll u, ll v)
{
    return tin[u] <= tin[v] && tout[v] <= tout[u];
}
bool h(ll u, ll v)
{
    return tin[v] <= tin[u] && tout[u] <= tout[v];
}
ll lca(ll u, ll v)
{
    if(f(u, v))
    {
        return u;
    }
    if(h(u, v))
    {
        return v;
    }
    for(int i = 19; i >= 0; i--)
    {
        if(!f(p[u][i], v))
        {
            u = p[u][i];
        }
    }
    return p[u][0];
}
int main(){
    ios::sync_with_stdio(0);
    ll n, m;
    cin >> n >> m;
    g.resize(n);
    p.resize(n);
    c.resize(m);
    tin.resize(n);
    tout.resize(n);
    d.resize(n);
    for(int i = 0; i < n - 1; i++)
    {
        ll u, v;
        cin >> u >> v;
        u--;
        v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }
    for(int i = 0; i < m; i++)
    {
        cin >> c[i];
        c[i]--;
    }
    p[c[0]].push_back(c[0]);
    d[c[0]] = 0;
    dfs(c[0], -1);
    for(int k = 1; k < 20; k++)
    {
        for(int i = 0; i < n; i++)
        {
            p[i].push_back(p[p[i][k - 1]][k - 1]);
        }
    }
    /*for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < 20; j++)
        {
            cout << p[i][j] + 1 << ' ';
        }
        cout << endl;
    }*/
    ll cur = c[0];
    set<ll> bad;
    if(m == 1)
    {
        cout << "YES" << endl << c[0] + 1;
        return 0;
    }
    for(int i = 1; i < m; i++)
    {
        ll v = c[0];
        if(d[c[i]] % 2 != 0)
        {
            cout << "NO";
            return 0;
        }
            ll iskd = d[v] + d[c[i]] / 2;
            //cout << iskd << endl;
            ll kek = c[i];
            for(int j = 19; j >= 0; j--)
            {
                //cout << kek << ' ' << j << endl;
                if(d[p[kek][j]] > iskd)
                {
                    kek = p[kek][j];
                }
            }
            //cout << "MEMM" << kek << ' ' << cur << endl;
            ll cnt = p[kek][0];
            if(cnt == cur)
            {
                bad.insert(kek);
            }
            if(f(cur, cnt))
            {
                if(bad.size())
                {
                    ll mem = cnt;
                    for(int j = 19; j >= 0; j--)
                    {
                        if(d[p[mem][j]] > d[cur])
                        {
                            mem = p[mem][j];
                        }
                    }
                    if(bad.find(mem) != bad.end())
                    {
                        cout << "NO";
                        return 0;
                    }
                }
                cur = cnt;
                bad.clear();
                bad.insert(kek);
            }
            else
            {
                if(f(cnt, cur))
                {
                    if(f(kek, cur))
                    {
                        cout << "NO";
                    return 0;
                    }
                }
                else
                {
                    cout << "NO";
                    return 0;
                }
            }

    }
    cout << "YES" << endl << cur + 1;
    return 0;
}
/*
6 3
1 2
2 3
3 4
4 5
4 6
1 5 6


5
3
1 4
4 2
1 5
5 3
1 2 3


5 4
1 2
2 3
2 4
2 5
1 3 4 5

6 4
1 2
2 3
2 4
4 5
5 6
1 3 4 6
*/

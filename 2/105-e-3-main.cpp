#include <iostream>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

#define int long long

const int INF = 1e18 + 9;

vector<vector<int>> g;
vector<int> lst;
vector<bool> used;
unordered_set<int> s;

bool dfs0(int v) {
    used[v] = true;
    if (s.find(v) != s.end()) {
        return true;
    }
    bool f = false;
    for (int& x : g[v]) {
        if (!used[x]) {
            f |= dfs0(x);
        }
    }
    return f;
}

int maxi = -1;
int z = -1;

void dfs1(int v, int x) {
    used[v] = true;
    if (s.find(v) != s.end()) {
        if (x > maxi) {
            z = v;
        }
        maxi = max(maxi, x);
    }
    for (int& y : g[v]) {
        if (!used[y]) {
            dfs1(y, x + 1);
        }
    }
}

bool fl = false;
int p = -1;

void dfs2(int v, int x) {
    used[v] = true;
    for (int& y : g[v]) {
        if (y == z) {
            fl = true;
        }
        if (!fl && !used[y]) {
            dfs2(y, x + 1);
        }
    }
    if (fl && x == maxi / 2) {
        p = v;
    }
}

unordered_set<int> st;

void dfs(int v, int x) {
    used[v] = true;
    if (s.find(v) != s.end()) {
        st.insert(x);
    }
    for (int& y : g[v]) {
        if (!used[y]) {
            dfs(y, x + 1);
        }
    }
}

signed main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n, m;
    cin >> n >> m;
    if (m == 1) {
        cout << "YES\n";
        cout << 1;
        return 0;
    }

    g.resize(n);

    for (int i = 0; i < n - 1; i++) {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        g[a].push_back(b);
        g[b].push_back(a);
    }

    lst.resize(m);
    for (int i = 0; i < m; i++) {
        cin >> lst[i];
        lst[i]--;
        s.insert(lst[i]);
    }
    used.resize(n);
    int cnt = 0;
    used[lst[0]] = true;
    int u = -1;
    for (int& x : g[lst[0]]) {
        if (dfs0(x)) {
            cnt++;
            u = x;
        }
    }
    if (cnt > 1) {
        cout << "NO";
        return 0;
    }
    used.assign(n, false);
    used[lst[0]] = true;
    dfs1(u, 1);

    if (maxi & 1) {
        cout << "NO";
        return 0;
    }
    used.assign(n, false);
    used[lst[0]] = true;
    dfs2(u, 1);

    used.assign(n, false);
    used[lst[0]] = true;
    dfs(p, 0);
    if (st.size() == 1) {
        cout << "YES\n";
        cout << p + 1;
        return 0;
    }
    cout << "NO";
    return 0;
}
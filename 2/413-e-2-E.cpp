#include <bits/stdc++.h>
#ifdef VRI
#define err(...) fprintf(stderr, __VA_ARGS__), fflush(stderr)
#else
#define err(...) 42
#endif

typedef long long ll;

using namespace std;

void solve();
int main() {
#ifdef VRI
    freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}

vector<vector<int>> E;
vector<int> take;

vector<pair<int, int>> dp;
pair<int, pair<int, int>> ans(-1, make_pair(-1, -1));

void dfs(int v, int pr = -1) {
    vector<pair<int, int>> cur;

    cur.emplace_back(-1, -1);
    if (take[v]) cur.emplace_back(0, v);

    for (int to : E[v]) if (to != pr) {
        dfs(to, v);
        if (dp[to].first != -1)
            cur.emplace_back(dp[to].first + 1, dp[to].second);
    }

    sort(cur.rbegin(), cur.rend());

    if (take[v] && !cur.empty() && cur[0].first > 0) {
        ans = max(ans, {cur[0].first, {cur[0].second, v }});
    }
    if (cur.size() > 1 && cur[1].first > 0) {
        ans = max(ans, { cur[0].first + cur[1].first, { cur[0].second, cur[1].second }});
    }
    dp[v] = cur[0];
}

vector<int> path;
int len = -1;
int ansV;

void calc(int v, int pr = -1) {
    path.push_back(v);

    if (v == ans.second.second) {
        if (path.size() % 2 == 0) {
            puts("NO");
            exit(0);
        }
        ansV = path[(int)path.size() / 2];
    }

    for (int to : E[v]) if (to != pr) {
        calc(to, v);
    }
    path.pop_back();
}

void check(int v, int pr = -1) {
    path.push_back(v);

    if (take[v]) {
        if (len == -1) len = (int) path.size();
        if (len != (int) path.size()) {
            puts("NO");
            exit(0);
        }
    }
    for (int to : E[v]) if (to != pr)
        check(to, v);
    path.pop_back();

}

void solve() {
    int n, k;
    cin >> n >> k;

    E.resize(n);
    take.resize(n);
    dp.resize(n);

    for (int i = 1; i < n; i++) {
        int a, b;
        cin >> a >> b;
        E[a - 1].push_back(b - 1);
        E[b - 1].push_back(a - 1);
    }

    for (int i = 0; i < k; i++) {
        int v; cin >> v;
        take[v - 1] = 1;
        if (k == 1) {
            cout << "YES\n";
            cout << v << "\n";
            return;
        }
    }
    dfs(0);

    calc(ans.second.first);
    check(ansV);
    cout << "YES\n";
    cout << ansV + 1 << "\n";
}
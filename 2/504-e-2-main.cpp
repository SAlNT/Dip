#include <bits/stdc++.h>

using namespace std;

vector<vector<int>> Vec(200100);
vector<bool> us(200100, false);
set<int> S;

int dfs(int dept, int v) {
    us[v] = true;
    int ans = -1;
    if (S.count(v)) {
        ans = dept;
    }
    for (int i = 0; i < int(Vec[v].size()); i++) {
        int a = Vec[v][i];
        if (us[a])
            continue;
        int c = dfs(dept + 1, a);
        ans = max(c, ans);
    }
    return ans;
}

pair<int, int> dfs1(int dept, int v, int par, int x) {
    if (dept == x){
        if (S.count(v)){
            return {v, -1};
        }
    }
    int ans = -1;
    for (int i = 0; i < int(Vec[v].size()); i++) {
        int a = Vec[v][i];
        if (a == par)
            continue;
        pair<int, int> c = dfs1(dept + 1, a, v, x);
        if (c.second != -1)
            return c;
        if (c.first != -1) {
            if (dept * 2 == x) {
                return {1, v};
            }
            return c;
        }
    }
    return {-1, -1};
}

bool dfs_check(int dept, int v, int par, int x) {
    if (S.count(v)) {
        return (dept == x);
    }
    bool ans = true;
    for (int i = 0; i < int(Vec[v].size()); i++) {
        int a = Vec[v][i];
        if (a == par)
            continue;
        ans = ans & dfs_check(dept + 1, a, v, x);
    }
    return ans;
}

int main() {
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < n - 1; i++) {
        int a, b;
        cin >> a >> b;
        a -= 1;
        b -= 1;
        Vec[a].push_back(b);
        Vec[b].push_back(a);
    }
    int a;
    for (int i = 0; i < m; i++) {
        cin >> a;
        a -= 1;
        S.insert(a);
    }
    if (m == 1) {
        cout << "YES" << endl << 1;
        return 0;
    }
    int x = dfs(0, a);
    if (x % 2 != 0) {
        cout << "NO";
        return 0;
    }
    pair<int, int> c = dfs1(0, a, -1, x);
    if (c.second == -1) {
        cout << "NO";
        return 0;
    }
    if (dfs_check(0, c.second, -1, x / 2)) {
        cout << "YES" << endl;
        cout << c.second + 1;
    } else {
        cout << "NO";
    }
    return 0;
}
#include <bits/stdc++.h>

#define forn(i, n) for(int i = 0; i < (n); i++)
#define fornr(i, n) for (int i = (n) - 1; i >= 0; i--)
#define forab(i, a, b) for (int i = (a); i < (b); i++)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

using namespace std;


typedef long long ll;
typedef long long LL;
typedef unsigned long long ull;
typedef unsigned long long ULL;
typedef pair<int, int> pii;
typedef vector<int> vi;

struct __TIMESTAMP{
    ~__TIMESTAMP() {
        cerr << "\n\n=====\n" << 1.0 * clock() / CLOCKS_PER_SEC << " sec.\n";
    }
} __timestamp;

const int maxn = 200179;
vector<int> tree[maxn];
bool mark[maxn];
int dist[maxn];

pair<int, int> max_dist_to_mark(int v, int p, int cd) {
    dist[v] = cd;
    pii ans = {-1, v};
    if (mark[v])
        ans = {cd, v};
    for (int u : tree[v]) {
        if (u == p)
            continue;
        ans = max(ans, max_dist_to_mark(u, v, cd + 1));
    }
    return ans;
}

vector<int> path;
bool path_dfs(int v, int w, int p) {
    path.pb(v);
    if (w == v)
        return true;
    for (int u : tree[v]) {
        if (u == p)
            continue;
        if (path_dfs(u, w, v))
            return true;    
    }
    path.pop_back();
    return false;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.precision(10);
    cout << fixed;
#ifdef LOCAL
    freopen(".in", "r", stdin);
    freopen(".out", "w", stdout);
#endif
    int n, m;
    cin >> n >> m;
    forn(i, n - 1) {
        int u, v;
        cin >> u >> v;
        u--; v--;
        tree[u].pb(v);
        tree[v].pb(u);
    }

    vector<int> mark_v(m);
    forn(i, m) {
        cin >> mark_v[i];
        mark_v[i]--;
        mark[mark_v[i]] = true;
    }

    int a = max_dist_to_mark(mark_v[0], -1, 0).second;
    int b = max_dist_to_mark(a, -1, 0).second;

    assert(path_dfs(a, b, -1));
    if (path.size() % 2 == 0) {
        cout << "NO" << endl;
        cerr << a + 1 << " " << b + 1 << " " << path.size() << endl;
        return 0;
    }

    int ans = path[path.size() / 2];
    int d = max_dist_to_mark(ans, -1, 0).first;
    for (int u : mark_v) {
        if (d != dist[u]) {
            cout << "NO" << endl;
            return 0;
        }
    }


    cout << "YES" << endl << ans + 1 << endl;
    return 0;
}
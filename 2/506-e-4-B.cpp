#include <bits/stdc++.h>

using namespace std;

const int MAXN = 2e5;

vector<int> g[MAXN];
int dist[MAXN];
int pr[MAXN];
bool used[MAXN];

void dfs_clear()
{
    for (int i = 0; i < MAXN; i++)
        pr[i] = -1;
}

void dfs(int v, int p)
{
    if (p == -1)
        dist[v] = 0;
    else
    {
        if (pr[p] == v)
            return;
        dist[v] = dist[p] + 1;
        pr[v] = p;
    }
    //cout << p + 1 << "->" << v + 1 << " dist: " << dist[v] << endl;
    for (int u: g[v])
        dfs(u, v);
}


int main()
{
    int m, n;
    int p, q;
    cin >> n >> m;
    if (m <= 1)
    {
        cout << "YES" << endl;
        cout << 1 << endl;
        return 0;
    }

    for (int i = 0; i < n - 1; i++)
    {
        cin >> p >> q;
        p--, q--;
        g[p].push_back(q);
        g[q].push_back(p);
    }

    vector<int> teams;
    for (int i = 0; i < m; i++)
    {
        cin >> p;
        teams.push_back(p - 1);
    }
    dfs_clear();
    dfs(teams[0], -1);
    int max_team = teams[0];
    for (int team: teams)
        if (dist[team] > dist[max_team])
            max_team = team;
    //cout << max_team + 1 << " " << dist[max_team] << endl;
    dfs_clear();
    dfs(max_team, -1);
    for (int team: teams)
        if (dist[team] > dist[max_team])
            max_team = team;
    //cout << max_team + 1 << " " << dist[max_team] << endl;
    int max_dist = dist[max_team];
    if (max_dist % 2)
    {
        cout << "NO";
        return 0;
    }

    int mid_dist = max_dist / 2;
    for (int i = 0; i < mid_dist; i++)
        max_team = pr[max_team];
    int mid = max_team;
    dfs_clear();
    dfs(mid, -1);
    for (int team: teams)
        if (dist[team] != mid_dist)
            {
                cout << "NO";
                return 0;
            }
    cout << "YES" << endl;
    cout << mid + 1 << endl;
    return 0;
}


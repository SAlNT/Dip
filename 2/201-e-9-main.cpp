#include <bits/stdc++.h>

using namespace std;

#define len(x) int(x.size())
#define all(x) x.begin(), x.end()

#define ff first
#define ss second

#pragma optimize("O3")

typedef long long ll;
typedef pair <int, int> pii;
typedef pair <ll, ll> pll;
typedef vector <int> masi;
typedef vector <ll> masl;

const int N = 2e5 + 10;

vector <int> g[N];

void bfs(vector <int> &d, vector <int> &p, int s, int n) {
    d.clear();
    d.resize(n, 1e9);
    d[s] = 0;
    p.clear();
    p.resize(n, -1);
    queue <int> q;
    q.push(s);
    while (!q.empty()) {
        int x = q.front();
        for (int j = 0; j < len(g[x]); j++) {
            int nw = g[x][j];
            if (d[nw] > d[x] + 1){
                d[nw] = d[x] + 1;
                p[nw] = x;
                q.push(nw);
            }
        }
        q.pop();

    }
}

vector <int> marked;

int n, m;

void read() {
 	cin >> n >> m;
 	for (int i = 0; i < n - 1; i++) {
 	 	int a, b;
 	 	cin >> a >> b;
 	 	a--;
 	 	b--;
 	 	g[a].push_back(b);
 	 	g[b].push_back(a);
 	}
 	marked.resize(m);
 	for (int i = 0; i < m; i++) {
 	 	int x;
 	 	cin >> x;
 	 	x--;
 	 	marked[i] = x;
 	}
}

int getv(){
 	int x = marked[0];
 	vector <int> d, p;
 	bfs(d, p, x, n);
 	int mx = -1;
 	int id = -1;
 	int ind = -1;
 	for (int i : marked) {
 		ind++;
 	 	if (mx < d[i]) {
 	 	 	mx = d[i];
 	 	 	id = ind;
 	 	}
 	}
 	return id;
}


bool solve() {
    if (m == 1) {
        cout << "YES\n1";
        return true;
    }
    int beg = 0;
    vector <int> p, d;
    bfs(d, p, marked[beg], n);
//    int keg = rand() % m;
    int keg = getv();
    while(keg == beg) { 
		keg = rand() % m;     	
    }
    int cur = marked[keg];
    vector <int> way;
    while (cur != marked[beg]) {
        way.push_back(cur);
        cur = p[cur];
    }
    way.push_back(cur);
    if (len(way) % 2 == 0) {
		return false;
    }
    int s = way[len(way) / 2];
    bfs(d, p, s, n);
    int dist = d[marked[0]];
    for (int i = 1; i < len(marked); i++) {
        int x = d[marked[i]];
        if (x != dist) {
            return false;
        }
    }
    cout << "YES\n" << s + 1;
    return true;
}

int main() {
    #ifdef HOME
    freopen("input.txt", "rt", stdin);
    #endif  //  HOME
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    srand(time(nullptr));
    read();
  	while(clock() < 1.95 * CLOCKS_PER_SEC)	{
  	 	if (solve()) {
  	 	 	return 0;
  	 	}
  	 	else {
  	 	 	break;
  	 	}
  	}
  	cout << "NO";
    return 0;
}

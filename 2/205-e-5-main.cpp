#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
using namespace std;

vector<int>g[200000];

int main(){
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    for(int i = 0; i < n-1; i++){
        int a, b;
        cin >> a >> b;
        a--; b--;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    vector<bool>used(n, false);
    queue<int>q;
    vector<int>cnt(n, 0);
    vector<int>time(n, 0);
    for(int i = 0; i < m; i++){
        int a;
        cin >> a;
        a--;
        q.push(a);
        used[a] = true;
        time[a]++;
        cnt[a]++;
    }
    if(m == 1){
        cout << "YES" << endl;
        cout << q.front() + 1;
        return 0;
    }
    while(!q.empty()){
        int v = q.front();
        q.pop();
        for(int i = 0; i < g[v].size(); i++){
            if(!used[g[v][i]]){
                q.push(g[v][i]);
                used[g[v][i]] = true;
                time[g[v][i]] = time[v] + 1;
                cnt[g[v][i]] = cnt[v];
            }else{
                if(time[g[v][i]] == time[v] - 1){

                }else if(time[v] + 1 == time[g[v][i]]){
                    cnt[g[v][i]] += cnt[v];
                    if(cnt[g[v][i]] == m){
                        cout << "YES" << endl;
                        cout << g[v][i] + 1;
                        return 0;
                    }
                }else{
                    cout << "NO";
                    return 0;
                }
            }
        }
    }
    cout << "NO";
    return 0;
}

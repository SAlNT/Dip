#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

ll back[200005];
ll step[200005];
ll memb[200005];
ll rmemb[200005];
bool been[200005];
ll mx_dist = 0;
ll n, m;

ll far_i, far_dist = -1;

vector<ll> g[200005];

void dfs(ll i, ll s) {
	//cout << "dfs " << i << endl;
    been[i] = true;
    step[i] = s;
    if(rmemb[i] != -1 && s > far_dist) {
    	far_dist = s;
    	far_i = i;
    }
	for (auto it : g[i]) {
		if (!been[it]) {
			back[it] = i;
			dfs(it, s+1);
		}

	}
}

ll back_step(ll i, ll s) {
	//cout << "back " << i << endl;
   if(s==0)
	   return i;
   return back_step(back[i], s-1);
}

int main() {
	cin >> n >> m;
	for(int i=0; i<n-1; i++) {
		int w, u;
		cin >> w >> u;
		g[w].push_back(u);
		g[u].push_back(w);
	}
	memset(rmemb, -1, sizeof(rmemb));
	for(int i = 0; i<m; i++) {
		cin >> memb[i];
		rmemb[memb[i]] = i;
	}
	dfs(memb[0], 0);
	//cout << "far " << far_dist << endl;
	ll j = back_step(far_i, far_dist/2);
	memset(step, 0, sizeof(step));
	memset(back, 0, sizeof(back));
	memset(been, 0, sizeof(been));
	dfs(j, 0);

	bool yes = true;
	for(int i = 1; i < m; i++) {
		if(step[memb[i]] != step[memb[i-1]]) {
			yes = false;
		}
	}
	if(yes) {
		cout << "YES" << endl;
		cout << j << endl;
	} else {
		cout << "NO" << endl;
	}

	//cout << far_i << " " << far_dist << endl;


	return 0;
}

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef long double ld;
const int maxn = (int)2e5 + 1;
vector <vector <ll> > g;
ll par[maxn], dist[maxn];
bool used[maxn];

inline void dfs(ll u, ll p = -1) {
    used[u] = true;
    par[u] = p;
    if (p == - 1)
        dist[u] = 0;
    else
        dist[u] = dist[p] + 1;
    for (int v : g[u])
        if (!used[v])
            dfs(v, u);
}

inline ll findRoot(ll u, ll k) {
    if (k == 0)
        return u;
    return findRoot(par[u], k - 1);
}

int main() {
    srand(time(nullptr));
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    ll n, m;
    cin >> n >> m;
    g.resize(n);
    for (int i = 1; i < n; ++i) {
        ll u, v;
        cin >> u >> v;
        u--, v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }
    ll mt[m];
    for (int i = 0; i < m; ++i) {
        cin >> mt[i];
        mt[i]--;
    }
    ll root = n / 2;
    ll niter = 0;
    bool used1[n];
    for (int i = 0; i < n; ++i)
        used1[i] = 0;
    if (n == m && n > 1) {
        cout << "NO";
        return 0;
    }
    while (niter < 221) {
        for (int i = 0; i < n; ++i)
            dist[i] = used[i] = 0;
        dfs(root);
        used1[root] = 1;
        ll mn = 1e9, mx = -1e9, mxId = -1;
        for (int i = 0; i < m; ++i) {
            mn = min(mn, dist[mt[i]]);
            if (mx < dist[mt[i]]) {
                mx = dist[mt[i]];
                mxId = mt[i];
            }
        }
        if (mn == mx) {
            cout << "YES" << '\n' << root + 1;
            return 0;
        }
        ll shift = (mx + mn + (rand() % 2)) / 2 - mn;
        ll k = mx - shift;
        root = findRoot(mxId, k);
        if (used1[root]) {
            cout << "NO";
            return 0;
        }
        niter++;
    }
    cout << "NO";
    return 0;
}
#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <queue>

using namespace std;

int ras[200000];
vector<int> g[200000];
set<int> city;
bool used[200000];
queue<int> q;

void bfs(int v) {
    q.push(v);
    while(!q.empty()) {
        int a = q.front();
        q.pop();
        for (auto i : g[a]) {
            if (!used[i]) {
                used[i] = true;
                q.push(i);
                ras[i] = ras[a] + 1;
            }
        }
    }
}

int bfs2(int v) {
    q.push(v);
    while (!q.empty()) {
        int a = q.front();
        q.pop();
        for (auto i : g[a]) {
            if (!used[i]) {
                q.push(i);
                used[i] = true;
                if (ras[i] != ras[a] + 1) {
                    city.erase(i);
                    if (city.empty()) {
                        cout << "NO";
                        return 1;
                    }
                }
                ras[i] = ras[a] + 1;
            }
        }
    }
}

int main()
{
    int m, n;
    cin >> n >> m;
    for (int i = 0; i < n; ++i) {
        city.insert(i);
        used[i] = false;
    }
    int a, b;
    for (int i = 0; i < n - 1; ++i) {
        cin >> a >> b;
        g[a - 1].push_back(b - 1);
        g[b - 1].push_back(a - 1);
    }

    cin >> a;
    ras[a - 1] = 0;
    used[a - 1] = true;
    bfs(a - 1);
    for (int i = 0; i < m - 1; ++i) {
        city.erase(a - 1);
        cin >> a;
        city.erase(a - 1);
        for (int j = 0; j < n; ++j) {
            used[j] = false;
        }
        used[a - 1] = true;
        ras[a - 1] = 0;
        if (bfs2(a - 1) == 1) {
            return 0;
        }
    }
    if (city.empty()) {
        cout << "NO";
    }
    else {
        cout << "YES\n";
        for (auto i : city) {
            cout << i + 1;
            return 0;
        }
    }
    return 0;
}

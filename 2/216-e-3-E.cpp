#include <iostream>

using namespace std;

long syties[200005];
long sytyes_paths[400005], syties_next[400005], syties_starts[200005], syties_end = 0;
long queque_a[200005], queque_b[200005], a_nd = 0, b_nd = 0;

int main()
{
	long N, M, i, ta, tb;
	long path_index, cur_s;
	long *cur_u, *next_u, * cur_end, * next_end, *tmp_u, *tmp_end;
	long last_place = 1;
	cur_u = queque_a;
	next_u = queque_b;
	cur_end = &a_nd;
	next_end = &b_nd;
	*cur_end = 0;
	*next_end = 0;
	syties_end = 0;
	cin >> N >> M;
	for (i = 1; i <= N; i++)
	{
		syties[i] = 300000;
		syties_starts[i] = -1;
	}
	for (i = 1; i < N; i++)
	{
		cin >> ta >> tb;
		sytyes_paths[syties_end] = tb;
		syties_next[syties_end] = syties_starts[ta];
		syties_starts[ta] = syties_end++;
		sytyes_paths[syties_end] = ta;
		syties_next[syties_end] = syties_starts[tb];
		syties_starts[tb] = syties_end++;
	}
	for (i = 0; i < M; i++)
	{
		cin >> ta;
		syties[ta] = 0;
		(next_u)[(*next_end)++] = ta;
	}
	while (*next_end > 1)
	{
		tmp_u = next_u;
		next_u = cur_u;
		cur_u = tmp_u;
		tmp_end = next_end;
		next_end = cur_end;
		cur_end = tmp_end;
		*next_end = 0;
		while (*cur_end > 0)
		{
			cur_s = (cur_u)[--(*cur_end)];
			path_index = syties_starts[cur_s];
			while (path_index != -1)
			{
				if (syties[sytyes_paths[path_index]] == syties[cur_s])
				{
					cout << "NO" << endl; return 0;
				}
				else if (syties[sytyes_paths[path_index]] > syties[cur_s])
				{
					if ((next_u)[(*next_end)] != sytyes_paths[path_index])
					{
						syties[sytyes_paths[path_index]] = syties[cur_s] + 1;
						(next_u)[(*next_end)++] = sytyes_paths[path_index];
					}
					
				}
				path_index = syties_next[path_index];
				last_place = cur_s;
			}
		}
	}
	cout << "YES" << endl << last_place << endl;


	return 0;
}
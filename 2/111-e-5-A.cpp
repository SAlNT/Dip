#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;


void BFS(vector<vector<int>> &G, int cur, vector<int> &col, vector<int> &path){
    queue<int> Q;
    Q.push(cur);
    col[cur] = 1;
    while(!Q.empty()){
        int u = Q.front();
       // cout << "take " << u << endl;
        Q.pop();
        for(int i = 0; i < G[u].size(); i++){
            if(col[G[u][i]] == 0){
                col[G[u][i]] = col[u] + 1;
               // cout << "PUSH " << G[u][i] << endl;
                Q.push(G[u][i]);
                path[G[u][i]] = u;
            }
        }
    }
    for(int i = 0; i < col.size(); i++){
        col[i] -= 1;
    }
}
void debug(int i){
    cout << "here " << i << endl;
}
int main(){
   // freopen("input.txt", "rt", stdin);
    int n, m;
    cin >> n >> m;
    vector<vector<int>> G(n);
    for(int i = 0; i < n - 1; i++){
        int a, b;
        cin >> a >> b;
        a--; b--;
        G[a].push_back(b);
        G[b].push_back(a);
    }
    vector<int> part(m);
    for(int i = 0; i < m; i++){
        cin >> part[i];
        part[i]--;
        //cout << part[i];
    }
    vector<int> col(n, 0);
    if(m == 1){
        cout << "YES" << endl;
        cout << part[0] + 1;
        return 0;
    }
    int st = part[0];
    int fin = part[1];
    //cout << fin << endl;
    vector<int> path(n, 0);
    BFS(G, st, col, path);
    for(int i = 2; i < m; i++){
        if(col[part[i]] > col[fin]){
            fin = part[i];
        }
    }
    if(col[fin] % 2 == 0){
        int mid = fin;
        int len = col[fin] / 2;
        while(len--){
            mid = path[mid];
        }
        vector<int> col1(n, 0);
        vector<int> path1(n, 0);
        BFS(G, mid, col1, path);
        int dis = col1[part[0]];
        for(int i = 0; i < m; i++){
            if(col1[part[i]] != dis){
                cout << "NO";
                return 0;
            }
        }
        cout << "YES" << endl;
        cout << mid + 1;

    } else {
        cout << "NO";
        return 0;
    }
}


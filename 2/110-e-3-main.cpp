#include <bits/stdc++.h>
using namespace std;

struct fastio{
	fastio(){
		ios::sync_with_stdio(0);
		cin.tie(0);
		cout << setprecision(12);
		cout << fixed;
		cerr << setprecision(5);
		cerr << fixed;
	}
}__________;

#ifdef LOCAL
#define debug(x) cerr << #x << " = " << x << endl;
#define ddebug(x, y) cerr << #x << ", " << #y << " = " << x << ", " << y << endl;
#else
#define debug(x);
#define ddebug(x, y);
#endif

typedef long long ll;
typedef long double ld;
typedef vector <int> vi;
typedef vector <ll> vl;
typedef pair <int, int> pii;
typedef pair <ll, ll> pll;

#define fi first
#define se second
#define sz(a) ((int)a.size())
#define pb push_back
#define mp make_pair
#define all(c) c.begin(), c.end()
#define sq(a) ((a) * (a))
#define FOR(i, a, b) for(int i = (int)(a); i < (int)(b); i++)
#define FORD(i, a, b) for(int i = (int)(a); i > (int)(b); i--)

const int MAX = 2e6 + 10;
const ll INF = 1e18 + 10;
const ll mod = 1e9 + 7;
const ld eps = 1e-8;
int n,m;
vector<int> g[200001];
int c[200001];
bool used[200001];
int d[200001];
int k[200001];
int d1[200001];
void dfs(int v){
 	used[v] =true;
 	for(int i =0 ;i < g[v].size();i++){
 	 	int to = g[v][i];
 	 	if(!used[to]){
 	 	 	d1[to] = d1[v]+1;
 	 	 	dfs(to);
 	 	}
 	}
}
void solve(){
	cin >>  n >> m;
	for(int i = 1;i <n;i++){
		int a,b;
		cin >> a >> b;
		g[a].pb(b);
		g[b].pb(a);
	}
	for(int i =0 ;i <m;i++){
	 	cin >> c[i];
	}
	queue<int> q;
	for(int i =0  ;i < m;i++){
	 used[c[i]] = true;
	 k[c[i]] = 1;
	 q.push(c[i]);
    }
    bool ok = true;
   	while(!q.empty()){
   	 	int v = q.front();
   	 	q.pop();
   	 //	used[v] = true;
   	 	ddebug(v,ok);
   	 	debug(d[v]);
   	 	for(int j =0 ;j < g[v].size();j++){
   	 	 	int to  = g[v][j];

   	 	 	if(!used[to]){
   	 	 	  	q.push(to);
   	 	 	  	used[to] = true;
   	 	 	  	d[to] = d[v]+1;
   	 	 	}
   	 	 	else {
   	 	 		if(d[to] >= d[v] && d[to]!= d[v]+1){
   	 	 	//	cout << v <<" "<< v <<" "<< d[to] <<" "<< d[v] <<"\n";
   	 	 			ok = false;
   	 	 		}
   	 	 	}
   	 	 	if(d[to] > d[v])k[to]+=k[v];
   	 	}
   	}	
   	int maxx = -1 ;
   	int r  = -1;
   	for(int i =1 ;i<= n;i++)
   		/*if(d[i] > maxx && k[i] == m){
   		 	maxx =d[i];
   		 	r =i; 
   		}*/
   		if(k[i] == m)r = i;
   	/*for(int i = 1;i <= n;i++)
   		used[i] = false;
   	dfs(r);
   	int y = d1[c[0]];
   	for(int i = 1;i < m;i++)
   		if(d1[c[i]]!=y)
   			ok = false;*/
   //	for(int i = 0 ;i< m;i++)
   	//	cout << c[i] <<" "<< d1[c[i]] <<"\n";
   	if(r == -1)
   		cout <<"NO";
   	else{
   	    cout <<"YES\n";
   	    cout << r;
   	}
}

int main(){
	clock_t beg = clock();
	solve();
	clock_t end = clock();
	ld elapsed_time = (ld)(end - beg) / CLOCKS_PER_SEC;
	debug(elapsed_time);
	return 0;
}
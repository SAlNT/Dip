n,m = map(int,input().split())
a = [[] for i in range(n)]

for i in range(n-1):
    x,y = map(int,input().split())
    a[x - 1].append(y)
    a[y - 1].append(x)

olymp = list(map(int,input().split()))

if m==1:
    print('YES')
    print(olymp[0])
else:
    q = [[olymp[0]-1,0]]
    j = [0,0]
    per = [[] for i in range(n)]
    while len(q)!=0:
        x = q.pop(0)
        if per[x[0]] == []:
            per[x[0]] = x[1]
            if j[1] < x[1] and (x[0]+1) in olymp:
                j = [x[0],x[1]]
            for i in a[x[0]]:
                if per[i-1] == []:
                    q.append([i-1,x[1]+1])

    q = [[j[0],0]]
    jj = [0,0]
    perr = [[] for i in range(n)]
    while len(q)!=0:
        x = q.pop(0)
        if perr[x[0]] == []:
            perr[x[0]] = x[1]
            if jj[1] < x[1] and (x[0]+1) in olymp:
                jj = [x[0],x[1]]

            for i in a[x[0]]:
                if perr[i-1] == []:
                    q.append([i-1,x[1]+1])

    if jj[1]%2 == 0:
        ans = (jj[1]//2)
        i = 0
        tmp = jj[1]
        q = [jj[0]]
        while i!=ans:
            x = q.pop(0)
            for j in a[x]:
                if perr[j-1] == tmp - 1:
                    tmp-=1
                    i+=1
                    q.append(j-1)
        ans = q[0] + 1
        if ans in olymp:
            print('NO')
        else:
            print('YES')
            print(ans)
    else:
        print('NO')
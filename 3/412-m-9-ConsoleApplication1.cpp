#// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <string>
#include <string.h>
#include<map>
#include <unordered_map>

#define forn(i,n) for(int (i) =0;(i)<(n);(i)++)
#define fore(i,k,n) for(int i =k;i<(n);i++)
#define ford(i,n) for(int i =n-1;i>=0;i--)
#define all(a) a.begin(),a.end()
using namespace std;
typedef long long ll;
typedef vector <int> vi;
typedef vector<vector<int>> vvi;
typedef vector<ll> vl;
typedef vector<vector<ll>> vvl;

ll step(int a, int n) {
	if (n == 1) return a;
	if (n % 2) return step(a, n - 1) * a;
	else {
		ll b = step(a, n / 2);
		return b * b;
	}
}

int main()
{
	int t;
	cin >> t;
	for (int k=0; k<t; k++) {
		int n, r=0;
		cin >> n;
		vector<int>a(n);
		//vector <int> pos(n);
		unordered_map <int, vector<int>> ma;
		//vector <int> vs(n);
		//vector <int> ind(2001, 0);
		forn(i, n) {
			cin >> a[i];
			ma[a[i]].push_back(i);
		}

		if (ma.size() == 1) {
			ll r = 1;
			if (n > 3) {
				for (int i = n - 2; i <= n; i++) {
					r *= i;
				}
				r /= 6;
			}
			cout << r<<'\n';
			continue;
		}

		if (ma.size() == 2) {
			ll r = 1;
			int raz = ma[a[0]].size();
			if (raz > 3) {
				for (int i = raz - 2; i <= raz; i++) {
					r *= i;
				}
				r /= 6;
			}
			else if (raz < 3) r = 0;

			ll r1 = 1;
			if (n - raz > 3) {
				for (int i = n - raz - 2; i <= n - raz; i++) {
					r1 *= i;
				}
				r1 /= 6;
			}
			else if (n - raz < 3) r1 = 0;
			
			
			cout << r + r1<<'\n';
			continue;
		}

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				int temp = 2*a[j] - a[i];
				int m = ma[temp].size()-1;
				while (m >= 0 && ma[temp][m] > j) {
					r++;
					m--;
				}
			}
		}

		

		cout << r<<'\n';
	}
	return 0;
}

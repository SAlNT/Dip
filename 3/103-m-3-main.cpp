#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(0);

    cin.tie(0);
    cout.tie(0);

    int t;
    //cin >> t;
    scanf("%d", &t);

    for (int k = 0; k < t; k++){
        int n, o = 0;
        //cin >> n;
        scanf("%d", &n);
        vector<int> m(n);
        for(int i = 0; i < n; i++)
            //cin >> m[i];
            scanf("%d", &(m[i]));
        for(int i = 0; i < n - 2; i++){
            for(int j = i + 1; j < n - 1; j++){
                for(int g = j + 1; g < n; g++){
                    if(m[g] - m[j] == m[j] - m[i])
                        o++;
                }
            }
        }
        //cout << o << endl;
        printf("%d\n", o);
    }
    return 0;
}

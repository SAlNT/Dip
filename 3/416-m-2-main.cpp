#include <bits/stdc++.h>

using namespace std;

int a[2005], b[2005];

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int t, n;

    cin >> t;
    for (int ii = 0; ii < t; ii++) {
        int ans = 0;

        cin >> n;
        for (int i = 0; i < n; i++)
            cin >> a[i];
        for (int i = 1; i < n - 1; i++) {
            for (int j = 0; j < n; j++) {
                b[j] = a[j];
            }
            sort(b, b + i);
            sort(b + i + 1, b + n);

            int p = i + 1;
            for (int j = i - 1; j >= 0 && p < n; j--) {
                while (p < n && b[p] - b[i] < b[i] - b[j]) p++;
                if (p == n || b[p] - b[i] != b[i] - b[j])
                    continue;
                int cnt = 0;
                while (p < n && b[p] - b[i] == b[i] - b[j]) {
                    p++;
                    cnt++;
                }
                int c = 0;
                while (j >= 0 && b[p - 1] - b[i] == b[i] - b[j]) {
                    c++;
                    j--;
                }
                ans += c * cnt;
                j++;
            }
        }
        cout << ans << "\n";
    }
    return 0;
}

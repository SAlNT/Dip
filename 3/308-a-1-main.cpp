#include <iostream>
#include <vector>

typedef long long ll;
typedef long double ld;

using namespace std;

int main() {
    int a, b, n;
    cin >> a >> b >> n;
    int bl, bp, ml, mp;
    bl = 0;
    bp = b;
    ml = 0;
    mp = a;
    int nh = 0;
    int d = b - a;
    while (bp < n || mp < n) {
        if (bp == mp) {
            //dv b
            bl += d;
            bp += d;
        } else {
            //dv m
            ml += d;
            mp += d;
        }
        nh++;
    }
    cout << nh;
    return 0;
}
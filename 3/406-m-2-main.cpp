#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

typedef long long int ll;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    int t;
    cin >> t;
    for (int u = 0; u < t; ++u) {
        int n;
        cin >> n;
        vector<int> a(n);
        for (int i = 0; i < n; ++i) {
            cin >> a[i];
        }
        unordered_map<int, int> mp;
        mp[a[0]]++;
        ll ans = 0;
        for (int i = 1; i < n - 1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                ans += mp[2 * a[i] - a[j]];
            }
            mp[a[i]]++;
        }

        cout << ans << "\n";
    }

    return 0;
}
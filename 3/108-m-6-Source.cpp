#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <cstdio>
#include <set>
#include <algorithm>

#define fork(i, n, step) for(int i=0; i<n; i+=step)

typedef unsigned long long ull;
typedef long long ll;

using namespace std;
int poisk_min(int n, int *mass) {
	int min = 1000000000;
	for (int i = 0; i < n;i++) if (mass[i] < min) min = mass[i];
	return min;
}
int poisk_max(int n, int* mass) {
	int max = 0;
	for (int i = 0; i < n; i++) if (mass[i] > max) max = mass[i];
	return max;
}
int kol_min(int n, int min, int* mass) {
	int kol = 0;
	for (int i = 0; i < n; i++) if (mass[i] == min) kol++;
	return kol;
}
int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);

	int t;
	cin >> t;
	fork (i, t, 1) {
		int n,res=0;
		cin >> n;
		int* mass = new int[n];
		fork(j, n, 1) scanf("%i", &mass[j]);
		int min = poisk_min(n, mass);
		int max = poisk_max(n, mass);
		for (int j = 1; j < n - 1; j++) {
			if (mass[j] != max && mass[j]!=min) {
				for (int z = j - 1; z >= 0; z--) {
					if (mass[j] == mass[z]) {
						for (int k = j + 1; k < n; k++)
							if (mass[k] == mass[z]) res++;
					}
					else if (mass[z] > 2 * mass[j])
						continue;
					else {
						for (int k = j + 1; k < n; k++)
							if (2 * mass[j] == mass[z] + mass[k]) {
								res++; break;
							}


					}
				}

			}

		}
		int kol = kol_min(n, min, mass);
		if (kol >= 3) res += (kol - 2) * (kol - 1) * (kol) / (2 * 3);
		cout << res << endl;
	}
	return 0;
}
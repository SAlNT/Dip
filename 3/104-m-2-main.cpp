#include <iostream>

int main() {
    int t;
    std::cin >> t;
    int n = 0;
    long long * a;
    long long amount[t];
    for (int f = 0; f < t; f++){
        amount[f] = 0;
        std::cin >> n;
        a = new long long[n];

        for (int i = 0; i < n; i++)
            std::cin >> a[i];

        for (int i = 0; i < n - 2; i++){
            for (int j = i + 1; j < n - 1; j++){
                for (int k = j + 1; k < n; k++){
                    if ((a[k] - a[j]) == (a[j] - a[i])){
                        amount[f]++;
                    }
                }
            }
        }

        a = NULL;
    }
    delete a;

    for (int i = 0; i < t; i++){
        std::cout << amount[i] << std::endl;
    }
    return 0;
}
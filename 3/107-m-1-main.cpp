#include <bits/stdc++.h>

using namespace std;

int main() {
    int q;
    cin >> q;
    for (int test = 0; test < q; test++) {
        int n;
        cin >> n;
        vector<int> a(n);
        for (int i = 0; i <n; i++) {
            cin >> a[i];
        }
        unordered_map<int,int> s;
        int ans = 0;
        for (int i = 0; i < n; i++) {
            for (int k = i+2; k < n; k++) {
                s[a[k-1]]++;
                if ((a[i]+a[k])%2==0) {
                    ans += s[(a[i]+a[k])/2];
                }
            }
            i++;
            s[a[i]]--;
            for (int k = n-1; k > i+1; k--) {

                if ((a[i]+a[k])%2==0) {
                    ans += s[(a[i]+a[k])/2];
                }
                s[a[k-1]]--;
            }
        }
        cout << ans << '\n';
        /*for (auto i:s) {
            cout << i.first << ' ' << i.second << '\n';
        }*/
    }
}
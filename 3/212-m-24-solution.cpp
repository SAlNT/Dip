#include <stdio.h>
#include <unordered_map>

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		int n;
		int ans = 0;
		scanf("%d", &n);
		int a[2001];
		for (int i = 0; i < n; ++i)
			scanf("%d", &a[i]);
		for (int i = 0; i < n; ++i) {
			std::unordered_map<int, int> q;
			for (int j = i + 1; j < n; ++j) {
				if ((((a[j] - a[i]) & 1) == 0)) {
					ans += q[(a[j] + a[i]) >> 1];
				}
				q[a[j]]++;
			}
		}
		printf("%d\n", ans);
	}
}
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<int, vector<int>> um;
int a[2001];

int main()
{
	int t;

	cin >> t;

	for (int l = 0; l < t; l++)
	{
		int n;

		cin >> n;

		um.clear();

		for (int i = 1; i <= n; i++)
		{
			cin >> a[i];

			um[a[i]].resize(n + 1);
			um[a[i]][i] = 1;
		}

		for (auto& p : um)
		{
			vector<int>& v = p.second;

			for (int i = (int)v.size() - 2; i >= 0; i--)
				v[i] += v[i + 1];
		}


		long long ans = 0;

		for (int i = 1; i + 1 < n; i++)
			for (int j = i + 1; j < n; j++)
			{
				int need = 2 * a[j] - a[i];

				if (um.find(need) != um.end())
					ans += um[need][j + 1];
			}
			

		cout << ans << endl;
	}


	return 0;
}
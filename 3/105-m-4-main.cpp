#include <iostream>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <algorithm>

using namespace std;

#define int long long

signed main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int q;
    cin >> q;

    while (q--) {
        int n;
        cin >> n;
        vector<int> a(n);
        for (int& x : a)
            cin >> x;
        int ans = 0;
        for (int i = 1; i < n; i++) {
            int j = i - 1;
            while (j > 0 && a[j - 1] > a[j]) {
                swap(a[j - 1], a[j]);
                j--;
            }
            for (int k = i + 1; k < n; k++) {
                int s = a[k] - a[i];
                int h = upper_bound(a.begin(), a.begin() + i, a[i] - s) - lower_bound(a.begin(), a.begin() + i, a[i] - s);
                ans += h;
            }
        }
        cout << ans << '\n';
    }


    return 0;
}
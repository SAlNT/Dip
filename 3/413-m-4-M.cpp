#include <bits/stdc++.h>
#ifdef VRI
#define err(...) fprintf(stderr, __VA_ARGS__), fflush(stderr)
#else
#define err(...) 42
#endif

typedef long long ll;

using namespace std;

void solve();
int main() {
#ifdef VRI
    freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int t; cin >> t;
    while (t--) solve();
    return 0;
}

void solve() {
    int n; cin >> n;
    vector<int> A(n);
    for (int &i : A) cin >> i;
    unordered_map<int, int> cnt;
    ll ans = 0;
    for (int j = n - 1; j > 0; --j) {
        for (int i = 0; i < j; ++i) {
            ans += cnt[2 * A[j] - A[i]];
        }
        cnt[A[j]]++;
    }
    cout << ans << "\n";
}
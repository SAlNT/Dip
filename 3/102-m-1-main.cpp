#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll mod = 998244353ll;
const int N = 2100;

int a[N];

void solve() {
    int n;
    scanf("%d", &n);

    std::unordered_map<int, int> cnt;
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
        ++cnt[a[i]];
    }

    ll ans = 0;

    for (int j = 0; j < n; ++j) {
        --cnt[a[j]];
        for (int i = 0; i < j; ++i) {
            int delta = a[j] - a[i];
            int need = delta + a[j];
            ans += cnt[need];
        }
    }

    printf("%lld\n", ans);
}

int main() {
    int t;
    scanf("%d", &t);

    while (t--) {
        solve();
    }
    return 0;
}
#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

const int MAX_N = 2001;
int a[MAX_N];


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int q;
    cin >> q;

    for (int i = 0; i < q; i++) {
        int n;
        int cnt = 0;
        cin >> n;
        unordered_map<int, int> m;
        for (int j = 0; j < n; j++) {
            cin >> a[j];
        }
        m[a[0]] += 1;
        for (int j = 1; j < n; j++) {
            for (int k = j + 1; k < n; k++) {
                int ai = 2 * a[j] - a[k];
                cnt += m[ai];


            }
            m[a[j]]++;
        }

        cout << cnt << '\n';

    }


    return 0;
}

#include <iostream>
#include <map>

using namespace std;

int main() {
	int t;
	cin >> t;
	long* ans = new long[t];
	for (int v = 0; v < t; v++) {
		ans[v] = 0;
		int n;
		cin >> n;
		long* a = new long[n];

		for (int i = 0; i < n; i++) {
			cin >> a[i];
		}

		map<long, int>* diff_after = new map<long, int>[n-1];

		for (int i = 1; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				diff_after[i][a[j] - a[i]]++;
			}
		}


		map<long, int>* diff_before = new map<long, int>[n-1];

		for (int i = 1; i < n - 1; i++) {
			for (int j = 0; j <= i-1; j++) {
				diff_before[i][a[i] - a[j]]++;
			}
		}

		for (int i = 1; i < n - 1; i++) {
			for (const auto& x : diff_before[i]) {
				ans[v] += x.second * diff_after[i][x.first];
			}
		}


		//for (int i = 1; i < n - 1; i++) {
		//	for (const auto& x : diff[i]) {
		//		cout << x.first << ": " << x.second << endl;
		//	}
		//	cout << endl << endl;
		//}

		//for (int i = 0; i < n - 2; i++) {
		//	for (int j = i + 1; j < n - 1; j++) {
		//		ans[v] += diff[j][a[j] - a[i]];
		//	}
		//}
		//
		delete[] diff_after;
		delete[] diff_before;
		delete[] a;
	}

	for (int i = 0; i < t; i++) {
		cout << ans[i] << endl;
	}

	return 0;
}
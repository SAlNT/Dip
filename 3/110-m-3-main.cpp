#include <bits/stdc++.h>
using namespace std;

struct fastio{
	fastio(){
		ios::sync_with_stdio(0);
		cin.tie(0);
		cout << setprecision(12);
		cout << fixed;
		cerr << setprecision(5);
		cerr << fixed;
	}
}__________;

#ifdef LOCAL
#define debug(x) cerr << #x << " = " << x << endl;
#define ddebug(x, y) cerr << #x << ", " << #y << " = " << x << ", " << y << endl;
#else
#define debug(x);
#define ddebug(x, y);
#endif

typedef long long ll;
typedef long double ld;
typedef vector <int> vi;
typedef vector <ll> vl;
typedef pair <int, int> pii;
typedef pair <ll, ll> pll;

#define fi first
#define se second
#define sz(a) ((int)a.size())
#define pb push_back
#define mp make_pair
#define all(c) c.begin(), c.end()
#define sq(a) ((a) * (a))
#define FOR(i, a, b) for(int i = (int)(a); i < (int)(b); i++)
#define FORD(i, a, b) for(int i = (int)(a); i > (int)(b); i--)

const int MAX = 2e6 + 10;
const ll INF = 1e18 + 10;
const ll mod = 1e9 + 7;
const ld eps = 1e-8;
pll a[MAX];           
void solve(){
	int T;
	cin >> T;
	FOR(___, 0, T){
		int n;
		cin >> n;
		debug(n);
		FOR(i, 0, n){
			cin >> a[i].fi;
			a[i].se = i;
		}
		sort(a, a + n);
		ll ans = 0;
		FOR(i, 0, n){
			if(a[i].fi == 2)
				debug(i);
			int l_d = i - 1, r_d = i + 1;
			while(l_d >= 0 && r_d < n){
				int l = l_d, r = r_d;
				int cnt_l = 0, cnt_r = 0;
				while(l >= 0 && r < n && 2ll * a[i].fi == a[l].fi + a[r].fi){
					if(a[l].se < a[i].se)
						cnt_l++;
					l--;
				}
				bool flag = false;
				if(l_d != l){
					flag = true;
					l++;
				}
				while(l >= 0 && r < n && 2ll * a[i].fi == a[l].fi + a[r].fi){
					if(a[r].se > a[i].se)
						cnt_r++;
					r++;
				}                    
				if(flag)
					l--;
                ans += (ll)cnt_l * cnt_r + (ll)((ll)l_d - l - cnt_l) * ((ll)r - r_d - cnt_r);
                if(a[i].fi == 2){
                	ddebug(cnt_l, cnt_r);
                	ddebug(l_d, r_d);
                	ddebug(l, r);
				}
                l_d = l, r_d = r;
                if(!(l_d >= 0 && r_d < n))
                	break;
				if(abs(a[r].fi - a[i].fi) <= abs(a[l].fi - a[i].fi))
					r_d++;
				else
					l_d--;
				
			}

		}
		cout << ans << "\n";
	}

}

int main(){
	clock_t beg = clock();
	solve();
	clock_t end = clock();
	ld elapsed_time = (ld)(end - beg) / CLOCKS_PER_SEC;
	debug(elapsed_time);
	return 0;
}
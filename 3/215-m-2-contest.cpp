#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

ll a[2000];

unordered_map<ll, ll> mapl, mapr;

int main() {
	int t;
	cin >> t;
	for (int i = 0; i < t; i++) {
		ll n;
		cin >> n;
		for (int j = 0; j < n; j++) {
			cin >> a[j];
		}
		ll sum = 0;
		for (int j = 1; j < n; j++) {
			mapr[a[j]]++;
		}
		for (int j = 0; j < n-1; j++) {
			for (auto it : mapl) {
				ll tmp = 2*a[j] - it.first;
				sum += mapr[tmp] * it.second;
			}
			//cout << j << " " << sum << endl;
			mapl[a[j]]++;
			mapr[a[j+1]]--;
		}
		mapl.clear();
		mapr.clear();
		cout << sum << endl;
	}

	return 0;
}

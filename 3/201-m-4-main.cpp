#include <bits/stdc++.h>

using namespace std;

#define len(x) int(x.size())
#define all(x) x.begin(), x.end()

#define ff first
#define ss second

#pragma optimize("O3")

typedef long long ll;
typedef pair <int, int> pii;
typedef pair <ll, ll> pll;
typedef vector <int> masi;
typedef vector <ll> masl;

const int N = 2001;

int a[N];

int main() {
    #ifdef HOME
    freopen("input.txt", "rt", stdin);
    #endif  //  HOME
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int t;
    cin >> t;
    unordered_map<int, int> was;
    while(t--) {
        int n;
        cin >> n;
        for (int i = 0; i < n; i++)
            cin >> a[i];
        ll ans = 0;
        was[a[0]] = 1;
        for (int j = 1; j < n - 1; j++) {
            for (int k = j + 1; k < n; k++) {
                int x = 2 * a[j] - a[k];
                ans += was[x];
            }
            was[a[j]]++;
        }
        was.clear();
        cout << ans << "\n";
    }
    return 0;
}

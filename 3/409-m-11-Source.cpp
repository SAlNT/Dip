#include <iostream>
#include <string>
#include <set>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <deque>
#include <algorithm>

#define ll long long
#define ull unsigned ll

#define MOD 1000000000+7

#define FOR(i,a,b) for(int i=a;i<b;i++)
#define LFOR(i,a,b) for(int i=b-1;i>=a;i--)


#define fori(n) for(int i = 0; i < n; i++)

using namespace std;



// B


/*int main() {
	int n;
	cin >> n;
	
	double ln = 2.0 / n;
	double l = -1.0, r = l + ln;

	int cnt = 0;

	fori(1000) {
		if (i % 62 == 1)
		cout << sin(i) << endl;
		/*
		double d = sin(i);
		if (l < d && d < r) {
			printf("%d\n", i);
			cnt++;
			l += ln;
			r += ln;
			if (cnt == n)
				break;
		}
		
	}

}*/



// M
//*
int main() {
	int t;
	cin >> t;
	for (int q = 0; q < t; q++) {
		int n = 0;
		cin >> n;
		vector<int> v(n);
		fori(n)
			cin >> v[i];

		map<int, int> present;
		for (int i = 0; i < n; i++)
			present[v[i]]++;

		int ans = 0;
		fori(n - 2) {
			present[v[i]]--;
			map<int, int> cp = present;
			for (int j = i + 1; j < n - 1; j++) {
				cp[v[j]]--;
				int dif = v[j] - v[i];
				ans += cp[v[j] + dif];
			}
		}
		cout << ans << endl;
	}

	return 0;
}
//*/

/*

4
5
1 2 1 2 1
3
30 20 10
5
1 2 2 3 4
9
3 1 4 1 5 9 2 6 5


*/


// I
/*

1
0 0 5


2
3 3 3
6 6 2


*/


vector<ll> A, Ap;

/*ll Ans(int cnt)
{
	ll c = 0;
	int st = 0;
	FOR(i, 0, Ap.size())
	{
		if (st > 0)
		{
			if (Ap[i] - Ap[st-1] > cnt)
			{
				c++;
				if (st == i)
					return -1;
				st = i;
			}
		}
		else
		{
			if (Ap[i] > cnt)
			{
				c++;
				if (st == i)
					return -1;
				st = i;
			}
		}
	}
	c++;
	return c;
}*/


/*int main()
{
	int n, m;
	cin >> n;
	A.resize(n);
	Ap.assign(n, 0);
	FOR(i, 0, n)
	{
		cin >> A[i];
		if (i > 0)
			Ap[i] = Ap[i - 1];
		Ap[i] += A[i];
	}
	cin >> m;
	FOR(i, 0, m)
	{
		ll z;
		cin >> z;
		ll ret = Ans(z);
		if (ret == -1)
			cout << "Impossible\n";
		else
			cout << ret << "\n";
	}
	return 0;
}
/*
6
4 2 3 1 3 4
8
10 2 5 4 6 7 8 8

5
1 1 1 1 1
2
5 6
*/


/*
bool comp(ll a, ll b)
{
	return a > b;
}

int main()
{
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		int n;
		cin >> n;

		vector <ll> v(n);
		for (int i = 0; i < n; i++)
			cin >> v[i];

		ll count = 0;

		for (int j = 1; j < n - 1; j++)
		{
			vector <ll> v2(v);
			for (int i = 0; i < j; i++)
				v2[i] = v[j] - v[i];
			for (int k = j + 1; k < n; k++)
				v2[k] = v[k] - v[j];
			sort(v2.begin(), v2.begin() + j);
			sort(v2.begin() + j + 1, v2.end());
			
			int l = 0;
			int r = n - 1;
			while (l < j && r > j)
			{
				ll countL = 0;
				ll countR = 0;
				while (v2[l + 1] == v2[l] && l + 1 < j)
				{
					countL++;
					l++;
				}
				while (v2[r - 1] == v2[r] && r - 1 > j)
				{
					countR++;
					r--;
				}
			}
		}

		cout << count << '\n';
	}

	return 0;
}

*/



/*void dfs(int st,int v)
{
	FOR(i, 0, Adj[v].size())
	{
		if (Adj[v][i] > 0)
		{
			if (used[i] == 0)
			{
				used[i]++;
				dfs(st,i);
			}
			else
			{
				used[i]++;
				if (Adj[st][i]>0 && used[i] > Adj[st][i])
				{
					used[i]--;
					Adj[st][i] = 0;
				}
			}
		}
	}
}*/


/*
vector<vector<int>> Adj,Ans;

int main()
{
	int n;
	cin >> n;
	Adj.resize(n);
	Ans.resize(n);
	FOR(i, 0, n)
	{
		char ch;
		Adj[i].resize(n);
		Ans[i].assign(n, 0);
		FOR(j, 0, n)
		{
			
			cin >> ch;
			Adj[i][j] = (int)(ch - '0');
		}
	}

	for(int i=n-1;i>=0;i--)
	{
		int kol = 0;
		
		for (int j = 0; j < i; j++)
		{
			if (Adj[j][i] > 0)
				kol = Adj[j][i];
		}
		

		for (int j = i - 1; j >= 0; j--)
		{
			if (Adj[j][i] > 0)
			{
				kol--;
				Ans[j][i] = 1;
				if (kol == 0)
					break;
			}
		}
	}

	FOR(i, 0, n)
	{
		FOR(j, 0, n)
		{
			cout << Ans[i][j];
		}
		cout << "\n";
	}

}
*/
/*
5
01113
00012
00001
00001
00000

5
01112
00011
00000
00001
00000

5
00111
00011
00001
00000
00000
*/
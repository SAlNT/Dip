#include <cstdio>
#include <iostream>

void main() {

	int ncase,n, counter;;
	scanf_s("%d", &ncase);
	for (int i = 0; i < ncase; i++) {
		counter = 0;
		scanf_s("%d" ,&n);
		int* arr = new int[n];
		for (int i = 0; i < n; i++)
			scanf_s("%d", &arr[i]);
		for (int i = 0; i < n-2; i++)
		{
			for (int j = i+1; j < n-1; j++)
			{

				for (int k = j+1; k < n; k++) {
					if (arr[j] - arr[i] == arr[k] - arr[j])
						counter++;
				}
			}
		}
		delete []arr;
		printf("%d\n", counter);
	}
}
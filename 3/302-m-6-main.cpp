#include <bits/stdc++.h>
#pragma optimize("O3")
#define ll long long
#define ld long double
#define pb push_back
#define all(v) v.begin(), v.end()
#define rep(i, n) for(int i = 0; i < n; i++)

using namespace std;

int doo[2005][2005];

int main() {
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    cout.precision(10);
    int t;
    cin >> t;
    int n;
    vector <int> a(2005);
    unordered_map<int,int> mp;
    while(t--) {
        mp.clear();
        cin >> n;
        ll res = 0;
        rep(i, n){
            cin >> a[i];
            mp[a[i]] = i + 1;
        }
        rep(i, n){
            // a[i] - chislo
            int x = a[i];
            doo[i][0] = 0;
            for(int j = 1; j < n; j++){
                // skolko do j (= a[i])
                doo[i][j] = doo[i][j-1] + (x == a[j-1]);
            }
        }
        rep(i, n){
            for(int j = i + 1; j < n; j++){
                int x = a[i] + a[j];
                if((x & 1) == 0){
                    int ind = mp[x >> 1];
                    if(ind == 0)
                        continue;
                    ind--;
                    int kol = doo[ind][j] - doo[ind][i+1];
                    res += kol;
                }
            }
        }
        cout << res << '\n';
    }
}
#include <iostream>
#include <vector>
#include <unordered_map>

typedef long long ll;
typedef long double ld;

using namespace std;

const int DEBUG = 0;

int n, m;
vector <vector <int> > g;
vector<int> homes;
vector<int> ishome;
vector<int> deleted;
vector<int> depth;
vector<int> parent;
bool valid = true;

void get_hard() {
    cout << "NO\n";
    exit(0);
}

int dfs1(int v, int p, int dep) {
    parent[v] = p;
    depth[v] = dep;
    int cnt = 0;
    bool need = false;
    for (int i : g[v]) {
        if (i != p) {
            int ans = dfs1(i, v, dep + 1);
            if (ans == 1) {
                need = true;
                cnt++;
            }
        }
    }
    if (g[v].size() == 1) {
        if (ishome[v] == 1) {
            return 1;
        } else {
            deleted[v] = 1;
            return 0;
        }
    } else {
        if (need && ishome[v] && p > -1) {
            valid = false;
        }
        if (p == -1) {
            if (cnt > 1) {
                valid = false;
            }
        }
        if (need) {
            return 1;
        } else {
            deleted[v] = 1;
            return 0;
        }
    }
}

void dfs2 (int v, int p, int dep) {
    depth[v] = dep;
    for (int i : g[v]) {
        if (i != p) {
            dfs2(i, v, dep+1);
        }
    }
}

int findhalf(int v) {
    int vn = v;
    for (int i = 0; i < (depth[vn]/2); i++) {
        v = parent[v];
    }
    return v;
}

int main() {
    cin >> n >> m;
    int v, u;
    g.resize(n, {});
    depth.resize(n, 0);
    for (int i = 0; i < n - 1; i++) {
        cin >> v >> u;
        v--, u--;
        g[v].push_back(u);
        g[u].push_back(v);
    }
    parent.resize(n, 0);
    homes.resize(m, 0);
    ishome.resize(n, 0);
    deleted.resize(n, 0);
    for (int i = 0; i < m; i++) {
        int tmp;
        cin >> tmp;
        tmp--;
        ishome[tmp] = 1;
        homes[i] = tmp;
    }
    if (m == 1) {
        cout << "YES\n" << homes[0];
        return 0;
    }
    dfs1(homes[0], -1, 0);
    if (!valid) {
        cout << "NO";
        return 0;
    }
    if (DEBUG) {
        for (int i = 0; i < n; i++) {
            cout << deleted[i] << " ";
        }
        cout << endl;
        for (int i = 0; i < n; i++) {
            cout << depth[i] << " ";
        }
        cout << endl;
        for (int i = 0; i < n; i++) {
            cout << ishome[i] << " ";
        }
        cout << endl;
    }
    int maxdep = 0;
    int maxdv = 0;
    for (int i = 0; i < n; i++) {
        if (ishome[i]) {
            if (depth[i] > maxdep) {
                maxdep = depth[i];
                maxdv = i;
            }
        }
    }
    if (maxdep % 2 == 1) {
        cout << "NO";
        return 0;
    }
    int vh = findhalf(maxdv);
    dfs2(vh, -1, 0);
    vector<int> resh;
    for (int i = 0; i < n; i++) {
        if (ishome[i] == 1) {
            resh.push_back(depth[i]);
        }
    }
    for (int i = 1; i < resh.size(); i++) {
        if (resh[i] != resh[0]) {
            cout << "NO";
            return 0;
        } else {
            cout << "YES\n" << vh+1;
            return 0;
        }
    }
    return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
using namespace std;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);


    int t;
    cin >> t;

    map<long long, int> uf;
    while(t--){
        int ans = 0;
        int n;
        cin >> n;
        vector<long long> v(n);
        for(int i = 0; i < n; i++){
            cin >> v[i];
        }
        uf.clear();
        for(int i = 1; i < n - 1; i++){
            if(uf.find(v[i - 1]) == uf.end()){
                uf[v[i - 1]] = 1;
            } else {
                uf[v[i - 1]] += 1;
            }
            for(int j = i + 1; j < n; j++){
                if(uf.find(2* v[i] - v[j]) != uf.end()){
                    ans += uf[2* v[i] - v[j]];
                }
            }
        }
        cout << ans << endl;
    }
}


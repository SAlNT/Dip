def main():
    t = int(input())

    for _ in range(t):
        n = int(input())
        l = [int(x) for x in input().split()]

        s = 0

        for i in range(1,n-1):
            ms = [l[i] - j for j in l[0:i]]


            for j in range(i+1, n):
                s += ms.count(l[j]-l[i])

        print(s)

    return 0
if __name__ == '__main__':
    main()
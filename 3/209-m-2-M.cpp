#include <bits/stdc++.h>

#define forn(i, n) for(int i = 0; i < (n); i++)
#define fornr(i, n) for (int i = (n) - 1; i >= 0; i--)
#define forab(i, a, b) for (int i = (a); i < (b); i++)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

using namespace std;


typedef long long ll;
typedef long long LL;
typedef unsigned long long ull;
typedef unsigned long long ULL;
typedef pair<int, int> pii;
typedef vector<int> vi;

struct __TIMESTAMP{
    ~__TIMESTAMP() {
        cerr << "\n\n=====\n" << 1.0 * clock() / CLOCKS_PER_SEC << " sec.\n";
    }
} __timestamp;

const int MAXN = 2010;

// unordered_map<int, int> Map;
int a[MAXN], b[MAXN], cnt[MAXN];

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.precision(10);
    cout << fixed;
#ifdef LOCAL
    freopen(".in", "r", stdin);
    freopen(".out", "w", stdout);
#endif

    int T;
    scanf("%d", &T);
    while(T--) {
        LL ans = 0;

        int n;
        scanf("%d", &n);
        forn(i, n) scanf("%d", &a[i]), b[i] = a[i];
        sort(b, b + n);
        int n1 = unique(b, b + n) - b;

        forn(i, n) {
            forn(j, n1) cnt[j] = 0;
        //    Map.clear();
            forab(k, i + 1, n) {
                int z = a[i] + a[k];
                if (z % 2 == 0) {
                    int pos = lower_bound(b, b + n1, z / 2) - b;
                    if (b[pos] == z / 2) ans += cnt[pos];
//                    ans += Map[z / 2];
                }
                int pos = lower_bound(b, b + n1, a[k]) - b;
                cnt[pos]++;
//                Map[a[k]]++;
            }
        }
        printf("%lld\n", ans);

    }
}
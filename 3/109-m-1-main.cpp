#include <bits/stdc++.h>

using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        vector<int> v(n);
        for (auto &x : v) {
            cin >> x;
        }
        unordered_map<int, int> prefix;
        unordered_map<int, int> suffix;
        for (auto x : v) {
            suffix[x]++;
        }
        int64_t ans = 0;
        for (auto x : v) {
            suffix[x]--;
            for (auto par : prefix) {
                ans += suffix[2 * x - par.first] * par.second;
            }
            prefix[x]++;
        }
        cout << ans << '\n';
    }
    return 0;
}
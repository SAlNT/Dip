#include <bits/stdc++.h>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        vector<int> a(n);
        for (auto& x : a) {
            cin >> x;
        }
        unordered_map<int, int> mp;
        long long answer = 0;
        for (int j = n - 1; j >= 0; --j) {
            for (int i = 0; i < j; ++i) {
                answer += mp[a[j] + a[j] - a[i]];
            }
            ++mp[a[j]];
        }
        cout << answer << '\n';
    }
    return 0;
}
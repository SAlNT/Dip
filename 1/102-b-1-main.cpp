#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const int MOD = 10;
const int N = 510;
const double PI = atan2(0, -1);


int main() {

    int start = 52174;

    int n;
    scanf("%d", &n);

//    double best = 1009;
//    int bestN = 0;
//    int bestM = 0;
//
//    for (int num = 1; num <= 40000; ++num) {
//        for (int denom = 1; denom <= 10000; ++denom) {
//            double cur = (double)num / (double)denom;
//            if (cur > PI && cur < best) {
//                best = cur;
//                bestN = num;
//                bestM = denom;
//            }
//        }
//    }
//
//    std::cout << bestN << ' ' << bestM << ' ' << best - PI << '\n';

    std::vector<std::pair<int, double>> ans;

    for (int i = 0; i < n; ++i) {
        ans.push_back(std::make_pair(start, sin(start)));
        start += 2 * 355;
    }

    for (int i = 1; i < n; ++i) {
        if (ans[i].second < ans[i - 1].second) {
            throw 228;
        }
    }
    for (int i = 0; i < n; ++i) {
        printf("%d\n", ans[i].first);
    }

//    double ans = 100;
//    int ansI;
//    for (int i = 0; i < 1000000; ++i) {
//        double s = sin(i);
//        if (s < ans) {
//            ans = s;
//            ansI = i;
//        }
//    }
//
//    std::cout << ansI << ' ' << ans;

    return 0;
}
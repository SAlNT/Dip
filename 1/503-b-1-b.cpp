#include <bits/stdc++.h>

#define sz(a) ((int)((a).size()))
#define char unsigned char
#define X first
#define x first
#define Y second
#define y second

using namespace std;

typedef long long ll;
typedef long double ld;

int main()
{
#ifdef ONPC
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif // ONPC
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t;
    for (int i = 1;; i++)
    {
        if (0.0 < sin((ld)i) && sin((ld)i) < 7e-5 && cos((ld)i) > 0)
        {
            t = i;
            break;
        }
    }
    int n;
    cin >> n;
    for (ll i = -(n / 2); i < (n + 1) / 2; ++i)
    {
        cout << i * t << '\n';// ' ' << sin((ld)(i * t)) << endl;
    }
    return 0;
}

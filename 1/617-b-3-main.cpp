#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fi first
#define se second

using namespace std;
typedef long long ll;
typedef long double ld;

ll mod = 1e9 + 7;
const int NUM = 1e6 + 5;
const ll INF = 1e18;

int main() {
    ios_base :: sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    //freopen("out.txt", "wt", stdout);

    int last = 5;
    ld e = 2e-5;

    /*for (int i = 6; i < 2e9; i++)
    {
      if (sin(last) < sin(i) && sin(i) < sin(last + e))
        cout << i << ",",
        last = i;
    }*/
    //cout.precision(20);
    /*pair<ld, int> ans = mp(1, -1);
    for (int i = -1e5; i < 1e5; i++)
        if (i != 0)
            ans = min(ans, mp((ld) abs(sin(i)), i));
    cout << ans.se << ' ' << sin(ans.se);*/
    int n;
    cin >> n;
    vector<int> ans;
    int i = 0;
    while(ans.size() < n){
        ans.pb(-355 * i * 2);
        if (ans.size() < n && i != 0)
            ans.pb(355 * i * 2);
        i++;
    }
    for (int i = 0; i < n; i++)
        cout << ans[i] << ' ';
    /*for (int i = 0; i < n / 2; i++) {
        if (sin(-355 * i * 2) < sin(-355 * (i - 1) * 2))
            cout << -355 * i * 2 << ' ';
        if (i)
    }*/
    return 0;
}
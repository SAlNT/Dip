//
// Created by qf on 26.10.2019.
//
#include <bits/stdc++.h>

using namespace std;

int is_pal(int i) {
    for (int j = 0; j < 6; j++) {
        if ((i >> j & 1) != (i >> (6 - j) & 1)) return 0;
    }
    return 1;
}
const double pi = acos(-1);
int main() {
    int n; cin >> n;
    int cnt = 0;
    for (int i = 0; i < 1e7 && cnt < n; i++) {
        if (sin(i * 18849) >= 0)
            cout << i * 18849 << "\n", cnt++;
    }
}
#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, j = 0;
    float k = 0, pi = 3.14;

    cin >> n;
    while(n > 0){
        if((float)(pi * j / 2 + 2*pi) > k && k < (float)(2*pi*k)){
            n--;
            cout << k << endl;
        }
        k += 1;
        j++;
    }
    return 0;
}

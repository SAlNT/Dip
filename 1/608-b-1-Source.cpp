#pragma GCC optimize("O3")

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <bitset>
#include <cstring>
#include <string>
#include <ctime>
#include <random>
#include <numeric>
#include <iomanip>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <cmath>

#define sz(a) (int)((a).size())
#define all(a) (a).begin(), (a).end()
#define pb push_back
#define eb emplace_back
#define X first
#define Y second

using namespace std;  using ll = long long; using pii = pair<int, int>; using vi = vector<int>; using ld = long double;

int32_t main() {
	ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);
	const ld PI = 3.1415926535897931;

	ld shft = PI / 2 / 25000;
	
	ld rot = 0;
	ld md = 0;
	int cnt = 0;

	vector<int> ans;

	for (int i = 1; cnt < 25000; i++) {
		rot += 1;
		if (rot > 2 * PI) rot -= 2 * PI;
		if (rot < PI && min(rot, PI - rot) > md && min(rot, PI - rot) < md + shft) {
			ans.pb(i);
			cnt++;
			md = min(rot, PI - rot);
		}
	}

	int n;
	cin >> n;

	cout << 0 << endl;
	for (int i = 0; i < n / 2; i++) cout << ans[i] << '\n';
	for (int i = 0; i < (n - 1) / 2; i++) cout << -ans[i] << '\n';

	//cerr << (ld)clock() / CLOCKS_PER_SEC << endl;
}
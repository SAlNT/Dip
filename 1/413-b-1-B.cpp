#include <bits/stdc++.h>
#ifdef VRI
#define err(...) fprintf(stderr, __VA_ARGS__), fflush(stderr)
#else
#define err(...) 42
#endif

typedef long long ll;

using namespace std;

void solve();
int main() {
#ifdef VRI
    freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}

void solve() {
    int n; cin >> n;
    int N = 50000;
    double c = -1 + 2.0 / N, olds = -1;
    int cnt = 0;
    for (int x = 0; cnt < n; ++x) {
        double J = sin(x);
        if (olds < J && J <= c) {
            cout << x << "\n";
            olds = J;
            c = olds + 4.4 / N;
            ++cnt;
            //if (cnt % 1000 == 0) err("%d %d %.5f\n", cnt, x, sin(x));
        }
    }
}
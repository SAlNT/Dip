#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cmath>
using namespace std;

double pi = 3.14159265358979;

double get(double minn){
    double delt = 1e-4;
    double delt2 = 1e-4;
    double delt3 = 1e-4;
    if(abs(minn) >= 2.0 / 3){
        return delt;
    }
    if(abs(minn) >= 1.0 / 3){
        return delt2;
    }
    return delt3;
}

int main(){
    int n;
    cin >>  n;
    double minn = -1;

    for(int i = -200000000; i <= 200000000; i++){
        if(sin(i) - minn < get(minn)){
            n--;
            cout << i << endl;
            minn = sin(i);

            if(n == 0){
                return 0;
            }
        }
    }
    cout << "nooo";
}

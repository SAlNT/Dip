import math
n = int(input())
a = -1
for i in range(n):
    p = math.asin(a)
    print(math.floor(p + 2 * math.pi * i))
    a = a + 0.000001

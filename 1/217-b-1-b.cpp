#include <bits/stdc++.h>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
#ifdef HOME
    freopen("in.txt", "rt", stdin);
#endif
    int n;
    cin >> n;
    int cur = 721;
    while (n--) {
        cout << cur << '\n';
        cur += 710;
    }
}

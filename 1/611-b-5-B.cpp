#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef long double ld;

mt19937 gen(chrono::high_resolution_clock::now().time_since_epoch().count());

vector<int> p[2], v[2];
vector<ld> a[2];
int n;

void solveMe() {
    vector<int> res;
    int last = INT_MIN;

    double prev = 0;
    int z = 710;
    cin >> n;

    vector <int> ans;
    if (n % 2 == 1)
        ans.push_back(0);

    for (int i = z, j = 0; j < n / 2; j++, i+=z) {
        ans.push_back(i);
        ans.push_back(-i);
    }

    for (int i: ans)
        cout << i << '\n';


    return;

    for (double x = -1; x <= 1;) {
        double cur = asin(x);
        double k = (last - cur) / 2;

    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    cout.setf(cout.fixed);
    cout.precision(8);
    solveMe();

    return 0;

    int n;
    cin >> n;
    v[0].resize(n + 1, -1);
    v[1].resize(n + 1, -1);
    a[0].resize(n + 3, INT_MAX);
    a[1].resize(n + 3, INT_MAX);
    p[1].reserve(n + 2);
    a[0][0] = INT_MIN;
    a[1][0] = INT_MIN;
    int iid[2] = {0, 0};
    for (int i = 1;; i += 2) {
        int j = i % 2;
        ld z = sin(i / 2);
        if (j)
            z = -z;
        int id = lower_bound(a[j].begin(), a[j].end(), z) - a[j].begin();
        a[j][id] = z;
        p[j].push_back(v[j][id - 1]);
        v[j][id] = i / 2;
        iid[j] = max(iid[j], id);
        if (iid[0] + iid[1] == n)
            break;
    }
    int w = (int) p[0].size() - 1;
    while (w > -1) {
        cout << w << '\n';
        w = p[0][w];
    }
    w = (int) p[1].size() - 1;
    while (w > -1) {
        cout << -w << '\n';
        w = p[1][w];
    }

    return 0;
}
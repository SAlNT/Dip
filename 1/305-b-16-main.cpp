#include <bits/stdc++.h>
#pragma GCC optimize("O3")

using namespace std;

int main() {
	ios::sync_with_stdio(0);
	cin.tie(0), cout.tie(0);

	auto pi = acos(-1);
	int n;
	cin >> n;
	int l = INT_MIN + 10, r = INT_MAX - 10;
	double cur = 1, eps = 2.0 / n;
	for (int i = 0; i < n; ++i) {
		if (i & 1) {
			while (cur - sin(l) > eps or cur <= sin(l)) {
				l += 3;
			}
			cur = sin(l);
			cout << l << '\n';
		} else {
			while (cur - sin(r) > eps or cur <= sin(r)) {
				r -= 3;
			}
			cur = sin(r);
			cout << r << '\n';
		}
		//cout << fixed << setprecision(10) << cur << '\n';
	}

	return 0;
}
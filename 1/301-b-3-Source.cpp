#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <vector>
#include <bitset>
#include <stack>
#include <queue>
#include <cstdio>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <random>
#include <iomanip>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <string>

using namespace std;

#define pb push_back
#define eb emplace_back
#define all(x) (x).begin(), (x).end()
#define len(x) (int)(x).size()

using ll = long long;
using ld = long double;

const ld pi = acos(-1.0L);
const ld eps = 0.00001L;

template<typename T, typename TT> void mi(T& a, TT b) {
	if (b < a)a = b;
}
template<typename T, typename TT> void ma(T& a, TT b) {
	if (b > a)a = b;
}

int main() {
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif

	int n;
	cin >> n;
	int best = -946041435;
	int step = 710;
	
	for (int i = 0; i < n; i++) {
		cout << best << "\n";
		best += step;
	}
	
	return 0;
}

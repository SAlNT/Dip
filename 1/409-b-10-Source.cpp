#include <iostream>
#include <string>
#include <set>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <deque>
#include <algorithm>

#define ll long long
#define ull unsigned ll

#define MOD 1000000000+7

#define FOR(i,a,b) for(int i=a;i<b;i++)
#define LFOR(i,a,b) for(int i=b-1;i>=a;i--)


#define fori(n) for(int i = 0; i < n; i++)

using namespace std;

// B


int main() {
	int n;
	cin >> n;

	double ln = 2.0 / n;
	double l = -1.0, r = l + ln;

	int cnt = 0;

	for(int i = 10000000; i < 100000000; i++) {
		double d = sin(i);
		if (l < d && d < r) {
			printf("%d\n", i);
			cnt++;
			l += ln;
			r += ln;
			if (cnt == n)
				break;
		}
	}

}
//
// Created by qf on 26.10.2019.
//

#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
typedef vector<ll> vll;

bool comp(pair<int, double>& a, pair<int, double>& b) {
    return a.second < b.second;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    if (getenv("DEBUG"))
        freopen("../in/b", "r", stdin);
    priority_queue<pair<double, int>> que;
    vll dyn;
    int n;
    cin >> n;
    const int lim = 2000000;
    dyn.resize(lim);
    ll x;
    double y;
    ll maxx = 0;
    double maxy = -1;
    for (int i = 0; i < lim; ++i){
        x = i;
        y = sin(i);
        que.push(pair(y,x));
        dyn[x] = y;
        if (y > maxy) {maxx = x; maxy = y;}
    }
    ll delta = 100;
    ll curx = maxx;
    double cury = maxy;
    //cout << curx << '\n';
    while (n--){
        auto p = que.top();
        que.pop();
        if (cury - dyn[p.second] < delta) {
            cout << p.second << '\n';
            curx = p.second;
            cury = dyn[curx];
        }
    }
    cout.flush();
}
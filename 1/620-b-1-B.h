#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef long double ld;

const ld PI = acos(-1);

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(0);

#ifdef DEBUG
    freopen("input.txt", "r", stdin);
#endif

    int n;
    cin >> n;

    const int MX = 50000;
    int x = 0;
    ld mn = 1000;
    for (int i = 0; i < MX; i++) {
        ld val = i - int((i / (2 * PI))) * 2 * PI;
        val += 4 * PI;
        while(val > 2 * PI) val -= 2 * PI;

        if (val < mn) {
            mn = val;
            x = i;
        }
    }

    vector<int> ans;
    for (int i = -25000; i <= 25000; i++) {
        ans.push_back(i * x);
    }

    for (int i = 0; i < n; i++) cout << ans[i] << '\n';


    return 0;
}
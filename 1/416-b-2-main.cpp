#include <bits/stdc++.h>

using namespace std;

const long double PI = 3.1415926535897932384626433;

int main()
{
    // freopen("test.in", "r", stdin);
    int n;
    cin >> n;

    int i = 0;
    for (int j = -573204; i < n; i++, j += 710) {
        cout << j << endl;
    }
    return 0;
}
